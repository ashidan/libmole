/**
 * @file
 * @brief PCDC UARTをラップするクラス
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 * @see https://renesas.github.io/fsp/group___u_s_b___p_c_d_c.html
 */
#if defined(__arm__)
#include "libmole/mole_config.h" // MOLE_CONFIG_PCDCUART
#if (MOLE_CONFIG_PCDCUART > 0U)

#include "libmole/mole_PcdcUart.hpp" // PcdcUart
#include "libmole/mole_Fifo.hpp" // Fifo
#include "libmole/mole_trap.h" // MOLE_ALWAYS()

#include "FreeRTOS.h"
#include "queue.h" // QueueHandle_t
#include "semphr.h" // SemaphoreHandle_t
#include "timers.h" // xTimerPendFunctionCallFromISR()

#include <cstring> // strlen()
#include <atomic> // atomic_flag
#include <new> // new(std::nothrow)

namespace mole {


/**
 * @brief PCDC UARTをラップするクラス
 * @details プラットフォームのPCDC UART APIを包んで、容易に使えるようにする。
 * 複数のタスクから1つのPCDC UARTへ送信することも考慮して、排他には
 * task notificationでなくSemaphoreを使用している。
 */
class PcdcUart : public ByteIo {
    static std::atomic_uint instances;

    const usb_instance_t* const m_usb;
    Fifo               m_txFifo;
    Fifo               m_rxFifo;
    TaskHandle_t       m_taskToNotify;
    uint8_t            m_rxBuf[64]; //!< R_USB_Read()で使う領域
    SemaphoreHandle_t  m_writeSem;
    TimerHandle_t      m_conf2writeDelay;
    bool               m_attached;
    uint32_t           m_indexToNotify;

    StaticSemaphore_t  m_writeSemEntity;
    StaticTimer_t      m_conf2writeDelayEntity;

public:
    PcdcUart(const PcdcUart&) = delete;
    PcdcUart& operator=(const PcdcUart&) = delete;
    PcdcUart(PcdcUart&& rhs) = delete;
    PcdcUart& operator=(PcdcUart&& rhs) = delete;

    /**
     * @brief コンストラクタ
     * @param[in] uart FSPのUSBインスタンス
     * @param[inout] txRing 送信リングバッファとして使う領域 !=nullptr
     * @param[inout] rxRing 受信リングバッファとして使う領域 !=nullptr
     * @param[in] indexToNotify Notifyに使うインデックス < configTASK_NOTIFICATION_ARRAY_ENTRIES
     * @note txRingおよびrxRingの寿命は、PcdcUartインスタンスと同じかより長い
     * 必要がある。もしringのほうが寿命が短い場合、不正な読み書きを引き
     * 起こす可能性がある。
     */
    PcdcUart(const usb_instance_t* usb,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify) noexcept;

    virtual ~PcdcUart() noexcept {}

    virtual void write(
        const uint8_t* buf,
        size_t bytes) noexcept override;

    virtual void write(
        const char* buf,
        size_t bytes) noexcept override;

    virtual void write(
        const char* buf) noexcept override;

    virtual size_t read(uint8_t* buf, size_t bytes) noexcept override;

    virtual size_t read(char* buf, size_t bytes) noexcept override;

    virtual uint8_t read() override;

    virtual int8_t readc() override;

    virtual void setReaderTask(TaskHandle_t task) noexcept override;

private:
    static void *operator new(size_t bytes, const std::nothrow_t&) noexcept
    {
        (void)bytes;
        static uintptr_t ram[MOLE_CONFIG_PCDCUART * sizeof(PcdcUart)/sizeof(uintptr_t)];

        const uint32_t myIndex = instances.fetch_add(1U);
        MOLE_ALWAYS(myIndex < MOLE_CONFIG_PCDCUART, myIndex);
        return &ram[myIndex * sizeof(PcdcUart)/sizeof(uintptr_t)];
    }

    /**
     * @brief newと対になる、nothrow用のdelete
     * @param ptr
     * @details new と対になる delete。デストラクタは暗黙では呼び出されない。
     */
    static void operator delete(void* ptr, const std::nothrow_t&) noexcept
    {
#if defined(MOLE_USE_UTEST)
        /* 必要であれば、ここでデストラクタに相当する処理を行う。
         * PcdcUart* const p = reinterpret_cast<PcdcUart*>(ptr);
         */

        (void)ptr;
        const uint32_t count = instances.fetch_sub(1U);
        MOLE_ALWAYS(count >= 1, count);
#else
        (void)ptr;
#endif
    }

    /**
     * @brief nothrow用deleteを用意した場合、デストラクタから呼ばれるふつうのdeleteも用意する必要がある
     * @param ptr 無視される
     */
    static void operator delete(void* ptr) noexcept
    {
        (void)ptr;
    }

    static void conf2writeDelayCb(TimerHandle_t handle) noexcept;
    static void callback(
        usb_event_info_t* info, usb_hdl_t cur_task,
        usb_onoff_t usb_state) noexcept;

public:
    static PcdcUart* create(const usb_instance_t* usb,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify) noexcept
    {
        return new(std::nothrow) PcdcUart(usb,
            txRing, rxRing,
            indexToNotify);
    }
#if defined(MOLE_USE_UTEST)
    static void destroy(ByteIo* uart) noexcept
    {
        PcdcUart::operator delete(uart, std::nothrow);
    }
#endif
};

std::atomic_uint PcdcUart::instances;

/**
 * コールバック関数のなかで使用する、PcdcUartのインスタンスへのポインタ
 * ユーザーの指定した情報をコールバック関数へ渡す方法が無く、かつ、
 * PcdcUartインスタンスは複数生成されることはないと決め打ちして、
 * 静的に持つことにする。
 */
static PcdcUart* f_PcdcUartInstance;

/* 静的メンバーメソッド
 * Timerタスクの中で呼ばれる */
void PcdcUart::conf2writeDelayCb(TimerHandle_t handle) noexcept
{
    PcdcUart* inst = static_cast<PcdcUart*>(pvTimerGetTimerID(handle));
    const usb_api_t* const api = inst->m_usb->p_api;

    /* この時点でapi->writeができるので、m_txFifoを調べて
     * ホストに送るべきデータがあるなら送信を開始、
     * 無いならセマフォを立てる。*/
    uint8_t *addr = nullptr;
    const size_t writable = inst->m_txFifo.getDmaTxCandidatePart(&addr);
    if (writable > 0U)
    {
        const auto err = api->write(
            inst->m_usb->p_ctrl, addr, writable, USB_CLASS_PCDC);
        MOLE_ALWAYS(FSP_SUCCESS == err, err);
    }
    else
    {
        const auto rc = xSemaphoreGive(inst->m_writeSem);
        MOLE_ALWAYS(pdTRUE == rc, rc);
    }
}

/* 静的メンバーメソッド
 * このコールバック関数は、割り込みコンテキストではなくPCD_TSKの中で呼ばれる。
 */
void PcdcUart::callback(
    usb_event_info_t* info, usb_hdl_t cur_task,
    usb_onoff_t usb_state) noexcept
{
    (void)cur_task;
    (void)usb_state;

    union LineCoding {
        usb_pcdc_linecoding_t s;
        uint8_t d[7];
    };
    static LineCoding linecoding;

    PcdcUart* const inst = f_PcdcUartInstance;
    const usb_api_t* const api = inst->m_usb->p_api;

    switch (info->event)
    {
        case USB_STATUS_READ_COMPLETE:
        {
            /* ここに到達した時点で、R_USB_Read()へ与えたバッファへ
             * 受信データが格納済み。*/
            inst->m_rxFifo.push(inst->m_rxBuf, info->data_size);

            // 受信待ちをしているタスクに対し、データの到着を伝える
            const auto rc = xTaskNotifyIndexed(
                inst->m_taskToNotify, inst->m_indexToNotify, 0U, eNoAction);
            MOLE_ALWAYS(pdTRUE == rc, rc);

            // 次の受信待ちを仕掛ける
            const auto err = api->read(inst->m_usb->p_ctrl,
                inst->m_rxBuf,
                sizeof(inst->m_rxBuf),
                USB_CLASS_PCDC);
            MOLE_ALWAYS(FSP_SUCCESS == err, err);
        }
        break;
        case USB_STATUS_WRITE_COMPLETE:
        {
            // 前回のwriteが終わったので、m_txFifoにそれを伝える
            inst->m_txFifo.markPoped();

            const size_t bytes = inst->m_txFifo.getStock();
            if (0U == bytes)
            {
                const auto rc = xSemaphoreGive(inst->m_writeSem);
                MOLE_ALWAYS(pdTRUE == rc, rc);
            }
            else
            {
                uint8_t *addr = nullptr;
                const size_t writable = inst->m_txFifo.getDmaTxCandidatePart(&addr);
                MOLE_ALWAYS(writable > 0U, writable);
                const auto err = api->write(
                    inst->m_usb->p_ctrl, addr, writable, USB_CLASS_PCDC);
                MOLE_ALWAYS(FSP_SUCCESS == err, err);
            }
        }
        break;
        case USB_STATUS_CONFIGURED:
        {
            // 初回の受信待ちを仕掛ける
            const auto err = api->read(inst->m_usb->p_ctrl,
                inst->m_rxBuf,
                sizeof(inst->m_rxBuf),
                USB_CLASS_PCDC);
            MOLE_ALWAYS(FSP_SUCCESS == err, err);

            /* USB_STATUS_CONFIGUREDに遷移してすぐR_USB_Write()をすると、
             * 2回目のR_USB_Write()をしたときになってから1回目のデータと一緒に
             * 届く。その問題の回避のため、待ってからR_USB_Write()する。*/
            const auto rc = xTimerStart(inst->m_conf2writeDelay, portMAX_DELAY);
            MOLE_ALWAYS(pdTRUE == rc, rc);
        }
        break;
        case USB_STATUS_REQUEST:
        {
            switch (info->setup.request_type & USB_BREQUEST)
            {
                case USB_PCDC_SET_LINE_CODING:
                {
                    // ホストから設定を貰って、その結果をlinecodingへ保存
                    const auto err = api->periControlDataGet(
                        inst->m_usb->p_ctrl,
                        linecoding.d, sizeof(linecoding.d));
                    MOLE_ALWAYS(FSP_SUCCESS == err, err);
                }
                break;
                case USB_PCDC_GET_LINE_CODING:
                {
                    // 現在のlinecodingを、ホストへ渡す
                    const auto err = api->periControlDataSet(
                        inst->m_usb->p_ctrl,
                        linecoding.d, sizeof(linecoding.d));
                    MOLE_ALWAYS(FSP_SUCCESS == err, err);
                }
                break;
                case USB_PCDC_SET_CONTROL_LINE_STATE:
                {
                    // 本来はここでRTS信号を制御する？
                    // 現在の状態を、ホストへ渡す
                    const auto err = api->periControlStatusSet(
                        inst->m_usb->p_ctrl, USB_SETUP_STATUS_ACK);
                    MOLE_ALWAYS(FSP_SUCCESS == err, err);
                }
                break;
                default:
                break;
            }
        }
        break;
        case USB_STATUS_REQUEST_COMPLETE:
        {
        }
        break;
        case USB_STATUS_DETACH: // ↓
        case USB_STATUS_SUSPEND:
        {
            inst->m_attached = false;
        }
        break;
        case USB_STATUS_RESUME:
        {
            inst->m_attached = true;
        }
        break;
        default:
        break;
    }
}

PcdcUart::PcdcUart(const usb_instance_t* usb,
    Buffer* txRing, Buffer* rxRing,
    uint32_t indexToNotify) noexcept :
    m_usb(usb),
    m_txFifo(txRing), m_rxFifo(rxRing),
    m_taskToNotify(xTaskGetCurrentTaskHandle()),
    m_rxBuf(),
    m_writeSem(nullptr), m_conf2writeDelay(nullptr),
    m_attached(false),
    m_indexToNotify(indexToNotify),
    m_writeSemEntity(), m_conf2writeDelayEntity()
{
    MOLE_ASSERTNN(usb);
    // ringとbytesの検査はFifoコンストラクタで行うので、ここでは行わない
    MOLE_ASSERT(indexToNotify < configTASK_NOTIFICATION_ARRAY_ENTRIES, indexToNotify);

    m_writeSem = xSemaphoreCreateBinaryStatic(&m_writeSemEntity);
    MOLE_ALWAYS(nullptr != m_writeSem, m_writeSem);

    /* CONFIGUREDからR_USB_Writeまでの時間待ち用タイマーを用意
     * する。xTimerStart()はCONFIGUREDのとき呼ぶので、ここでは
     * 呼ばない。 */
    m_conf2writeDelay = xTimerCreateStatic(
        "conf2write Tmr",

        /* 実験で決めた値 30msでは効果なし、40msで初回のR_USB_Write()
         * の文字がきちんと出せたので、マージンを考えその2倍とした。 */
        pdMS_TO_TICKS(80),

        pdFALSE,                       // uxAutoReload
        static_cast<void*>(this), // pvTimerID
        conf2writeDelayCb,
        &m_conf2writeDelayEntity);
    MOLE_ALWAYS(nullptr != m_conf2writeDelay, m_conf2writeDelay);

    const usb_api_t* const api = m_usb->p_api;

    /* usb->p_cfgのほうにはコールバック関数を書いていないので、
     * open()したあとにcallbackSet()の順で呼び出さないと
     * open()のせいでそれ以前のcallbackSet()が無効化されてしまう。 */
    auto err = api->open(m_usb->p_ctrl, m_usb->p_cfg);
    MOLE_ALWAYS(FSP_SUCCESS == err, err);

    // コールバック呼び出しに備え、ポインタを設定しておく
    f_PcdcUartInstance = this;

    err = api->callback(callback);
    MOLE_ALWAYS(FSP_SUCCESS == err, err);
}

void PcdcUart::write(
    const uint8_t* buf,
    size_t bytes) noexcept
{
    MOLE_ASSERTNN(buf);
    if (0U == bytes) return;

    const usb_api_t* const api = m_usb->p_api;

    for (;;)
    {
        const size_t actual = m_txFifo.push(buf, bytes);

        if (pdTRUE == xSemaphoreTake(m_writeSem, 0U))
        {
            /* セマフォを獲得できたということは転送は未起動なので、
             * このタイミングで起動する。 */
            uint8_t *addr = nullptr;
            const size_t writable = m_txFifo.getDmaTxCandidatePart(&addr);
            /* xSemaphoreTake()がリターンする前に、callback()のなかで
             * データ送出を終えている可能性があり、writable==0がありうる */
            if (writable > 0U)
            {
                const auto err = api->write(
                    m_usb->p_ctrl, addr, writable, USB_CLASS_PCDC);
                MOLE_ALWAYS(FSP_SUCCESS == err, err);
            }
            else
            {
                const auto rc = xSemaphoreGive(m_writeSem);
                MOLE_ALWAYS(pdTRUE == rc, rc);
            }
        }
        else
        {
            /* セマフォを獲得できなかったということは今は転送中であり、
             * 放っておけばcallbackが転送を再起動してくれるので何もしない。*/
        }

        if (actual >= bytes)
        {
            return;
        }

        // fifoの空きが十分でなく、押し込みきれなかったとき
        buf   += actual;
        bytes -= actual;

        /* 少し待って、push()に再挑戦する。
         * 本当は時間でなくイベントを待ちたいのであるが、適当な
         * ものがないので時間にしている。 */
        vTaskDelay(pdMS_TO_TICKS(1));
    }
}

void PcdcUart::write(
    const char* buf,
    size_t bytes) noexcept
{
    write(reinterpret_cast<const uint8_t*>(buf), bytes);
}

void PcdcUart::write(
    const char* buf) noexcept
{
    write(reinterpret_cast<const uint8_t*>(buf), std::strlen(buf));
}

size_t PcdcUart::read(uint8_t* buf, size_t bytes) noexcept
{
    size_t actual = 0U;
    for (;;)
    {
        actual = m_rxFifo.pop(buf, bytes);
        if (actual > 0)
        {
            break;
        }

        // 通知が来るまで寝て待つ
        ulTaskNotifyTakeIndexed(m_indexToNotify, pdTRUE, portMAX_DELAY);
    }
    return actual;
}

size_t PcdcUart::read(char* buf, size_t bytes) noexcept
{
    return read(reinterpret_cast<uint8_t*>(buf), bytes);
}

uint8_t PcdcUart::read()
{
    uint8_t data;
    read(&data, 1U);
    return data;
}

int8_t PcdcUart::readc()
{
    uint8_t data;
    read(&data, 1U);
    return static_cast<int8_t>(data);
}

void PcdcUart::setReaderTask(TaskHandle_t task) noexcept
{
    MOLE_ASSERTNN(task);
    m_taskToNotify = task;
}

namespace PcdcUartFactory {

    ByteIo* create(
        const usb_instance_t* usb,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify) noexcept
    {
        return PcdcUart::create(usb,
            txRing, rxRing,
            indexToNotify);
    }

#if defined(MOLE_USE_UTEST)
    void destroy(ByteIo *uart) noexcept
    {
        PcdcUart::destroy(uart);
    }
#endif
} // end of PcdcUartFactory

} // end of mole

#endif // MOLE_CONFIG_PCDCUART > 0U
#endif // __arm__
