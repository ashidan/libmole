/**
 * @file
 * @brief i2cマスター通信をする
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#if defined(__RX__)
#include "libmole/mole_I2c.hpp"
#include "libmole/mole_trap.h" // MOLE_ASSERT()
#include "libmole/mole_misc.hpp" // elementsOf()
#include <cstring> // memset()

namespace mole {

I2c::I2c(uint8_t ch_no) noexcept : m_info(), m_send(), m_slvaddr(0)
{
    m_info.dev_sts      = RIIC_NO_INIT;
    m_info.ch_no        = ch_no;
    switch (ch_no)
    {
#if RIIC_CFG_CH0_INCLUDED == 1
        case 0:
            inst0 = this; // コールバック関数の中で参照したいから
            m_info.callbackfunc = callback0;
            break;
#endif
#if RIIC_CFG_CH1_INCLUDED == 1
        case 1:
            inst1 = this; // コールバック関数の中で参照したいから
            m_info.callbackfunc = callback1;
            break;
#endif
#if RIIC_CFG_CH2_INCLUDED == 1
        case 2:
            inst2 = this; // コールバック関数の中で参照したいから
            m_info.callbackfunc = callback2;
            break;
#endif
        default:
            mole_abort(ch_no);
            break;
    }
    m_info.cnt2nd       = 0;
    m_info.cnt1st       = 0;
    m_info.p_data2nd    = reinterpret_cast<uint8_t*>(FIT_NO_PTR);
    m_info.p_data1st    = m_send;
    m_info.p_slv_adr    = &m_slvaddr;

    const riic_return_t rc = R_RIIC_Open(&m_info);
    MOLE_ALWAYS(RIIC_SUCCESS == rc, rc);
}

void I2c::send(uint8_t slvaddr,
    const uint8_t* s1, size_t s1bytes,
    const uint8_t* s2, size_t s2bytes) noexcept
{
    MOLE_ASSERTNN(s1);
    MOLE_ASSERT(s1bytes >= 1U, s1bytes);
    MOLE_ASSERTNN(s2);
    MOLE_ASSERT(s2bytes >= 1U, s2bytes);

    uint32_t i = 0;
    for (; i < elementsOf(m_send); i++)
    {
        if (i < s1bytes)
        {
            m_send[i] = s1[i];
        }
        else if (i < s2bytes)
        {
            m_send[i] = s2[i - s1bytes];
        }
        else
        {
            break;
        }
    }
    m_info.cnt1st = i;
    m_slvaddr = slvaddr;
    const riic_return_t rc = R_RIIC_MasterSend(&m_info);
}

void I2c::send(uint8_t slvaddr,
    const uint8_t* s, size_t sbytes) noexcept
{
    MOLE_ASSERTNN(s);
    MOLE_ASSERT(sbytes >= 1U, sbytes);

    uint32_t i = 0;
    for (; i < elementsOf(m_send); i++)
    {
        if (i < sbytes)
        {
            m_send[i] = s[i];
        }
        else
        {
            break;
        }
    }
    m_info.cnt1st = i;
    m_slvaddr = slvaddr;
    const riic_return_t rc = R_RIIC_MasterSend(&m_info);
}

void I2c::send(uint8_t slvaddr) noexcept
{}

void I2c::startAndStop(void) noexcept
{}

bool I2c::recv(uint8_t slvaddr,
    uint8_t* r, size_t rbytes) noexcept
{
    return true;
}

bool I2c::sendAndRecv(uint8_t slvaddr,
    const uint8_t* s, size_t sbytes,
    uint8_t* r, size_t rbytes) noexcept
{
    return true;
}


#if RIIC_CFG_CH0_INCLUDED == 1
I2c* I2c::inst0;
void I2c::callback0(void) noexcept
{
    riic_mcu_status_t status;
    const riic_return_t rc = R_RIIC_GetStatus(&inst0->m_info, &status);
    MOLE_ALWAYS(RIIC_SUCCESS == rc, rc);

    ;
}
#endif

#if RIIC_CFG_CH1_INCLUDED == 1
I2c* I2c::inst1;
void I2c::callback1(void) noexcept
{}
#endif

#if RIIC_CFG_CH2_INCLUDED == 1
I2c* I2c::inst2;
void I2c::callback2(void) noexcept
{}
#endif

}


#endif
