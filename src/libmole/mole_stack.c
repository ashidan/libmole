/**
 * @file
 * @brief FreeRTOSのTimerTaskのためにメモリを確保する
 * @details RA FSPのra_gen/main.cにあるvAppliactionGetTimerTaskMemory()
 * の実装では、Stackの大きさに configMINIMAL_STACK_SIZE を使っており、
 * IdleTaskと同一の設定になってしまう。
 * そこで、ra_gen/main.cの実装がweakなのを利用して、
 * configTIMER_TASK_STACK_DEPTH を使う関数を自作して使うことにした。
 * なお、TimerTaskのスタックを増やす目的は、
 * xTimerPendFunctionCall()の利用である。
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "FreeRTOS.h"
#include "timers.h" // vApplicationGetTimerTaskMemory()

void vApplicationGetTimerTaskMemory(
    StaticTask_t** ppxTimerTaskTCBBuffer,
    StackType_t**  ppxTimerTaskStackBuffer,
    uint32_t*      pulTimerTaskStackSize)
{
    static StaticTask_t xTimerTaskTCB;
    static StackType_t  uxTimerTaskStack[configTIMER_TASK_STACK_DEPTH];

    *ppxTimerTaskTCBBuffer   = &xTimerTaskTCB;
    *ppxTimerTaskStackBuffer = uxTimerTaskStack;
    *pulTimerTaskStackSize   = sizeof(uxTimerTaskStack)/sizeof(uxTimerTaskStack[0]);
}

#if defined(__RX__)
// RXの場合は、IdleTask用も自分で用意する必要がある

void vApplicationGetIdleTaskMemory(
    StaticTask_t** ppxIdleTaskTCBBuffer,
    StackType_t**  ppxIdleTaskStackBuffer,
    uint32_t*      pulIdleTaskStackSize)
{
    static StaticTask_t xIdleTaskTCB;
    static StackType_t  uxIdleTaskStack[configMINIMAL_STACK_SIZE];

    *ppxIdleTaskTCBBuffer   = &xIdleTaskTCB;
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;
    *pulIdleTaskStackSize   = sizeof(uxIdleTaskStack)/sizeof(uxIdleTaskStack[0]);
}

#endif
