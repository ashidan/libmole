/**
 * @file
 * @brief zForthとの橋渡しをする
 * @details
 * https://github.com/zevv/zForth/blob/master/src/atmega8/main.c
 * を参考にして作った、小規模マイコン用の実装。
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "libmole/mole_zForth.hpp"
#include "libmole/mole_trap.h" // MOLE_ASSERTNN()
#include "libmole/mole_misc.hpp" // elementsOf()
#include "zforth/zforth.h" // zf_init()
#include <cstdio> // snprintf()
#include <cstring> // strlen()

#if 0 == configUSE_MUTEXES
#   error configUSE_MUTEXES must be 1.
#endif

namespace mole {

static zForth* f_inst = nullptr;
static ByteIo* f_io   = nullptr;

/**
 * zForthのインスタンシエート時点で解釈させるzForthスクリプト。
 * 末尾の番兵を除き、各行を\0で終える必要がある。
 */
static const char* const f_script =
": emit 0 sys ;\0" // ZF_SYSCALL_EMIT
": . 1 sys ;\0"    // ZF_SYSCALL_PRINT
": tell 2 sys ;\0"    // ZF_SYSCALL_TELL
""; // 番兵

static constexpr int32_t f_userFuncBase = 128;

zForth::zForth(ByteIo* io, const char* words) noexcept :
    m_mutex(), m_mutexEntity(), m_lastUserFunc(f_userFuncBase-1)
{
    MOLE_ASSERTNN(io);
    MOLE_ASSERTNN(words);
    m_mutex = xSemaphoreCreateMutexStatic(&m_mutexEntity);
    MOLE_ASSERTNN(m_mutex);

    /* グローバル変数 f_io f_inst を使用するので、複数のタスクからzForth
     * インタープリターを使うことを想定すると排他が必要になる。 */
    auto rc = xSemaphoreTake(m_mutex, portMAX_DELAY);
    MOLE_ALWAYS(pdTRUE == rc, rc);
    f_io   = io;
    f_inst = this;

    zf_init(1);
    zf_bootstrap();

    const char* pos = f_script;
    while (*pos != '\0')
    {
        zf_eval(pos);
        pos += std::strlen(pos) + 1U; // 次行の先頭へ
    }

    pos = words;
    while (*pos != '\0')
    {
        char buf[32];
        m_lastUserFunc++;
        MOLE_ALWAYS(m_lastUserFunc <= 255, m_lastUserFunc);
        const auto len = std::strlen(pos);
        MOLE_ALWAYS(len <= 16, len);
        std::snprintf(buf, sizeof(buf), ": %s %ld sys ;", pos, m_lastUserFunc);
        zf_eval(buf);
        pos += len + 1U; // 次行の先頭へ
    }

    rc = xSemaphoreGive(m_mutex);
    MOLE_ALWAYS(pdTRUE == rc, rc);
}

zForth::zForth(ByteIo* io) noexcept :
    zForth(io, "")
{
}

void zForth::interpret(ByteIo* io, const char* buf) noexcept
{
    MOLE_ASSERTNN(io);
    MOLE_ASSERTNN(buf);

    // 長さ0のときはzForthに解釈させずに単にリターンする
    if ('\0' == buf[0]) return;

    /* グローバル変数 f_io f_inst を使用するので、複数のタスクからzForth
     * インタープリターを使うことを想定すると排他が必要になる。 */
    auto rc = xSemaphoreTake(m_mutex, portMAX_DELAY);
    MOLE_ALWAYS(pdTRUE == rc, rc);
    f_io   = io;
    f_inst = this;
    const zf_result result = zf_eval(buf);
    rc = xSemaphoreGive(m_mutex);
    MOLE_ALWAYS(pdTRUE == rc, rc);

    if (ZF_OK != result)
    {
        io->write("A\r\n");
    }
    else
    {
        /* FORTHの流儀だとふつうは改行をしないのであるが、ここで改行
         * しないとせっかくの出力が入力行で上書きされてしまうので、
         * ここで改行する。 */
        io->write("\r\n");
    }
}

void zForth::userFunc(ByteIo* io, int32_t userFunc, const char* input) noexcept
{
    (void)io;
    (void)userFunc;
    (void)input;
#if 0
    switch (userFunc)
    {
        case UF_SYSCALL_FOOBAR:
            何らかの実装
            break;
        default:
            break;
    }
#endif
}

}

extern "C" {

zf_input_state zf_host_sys(zf_syscall_id id, const char* input)
{
    MOLE_ASSERTNN(mole::f_io);
    MOLE_ASSERTNN(mole::f_inst);
    // 正常な処理フローにおいても、inputはnullptrになりうる

    switch (id)
    {
        case ZF_SYSCALL_EMIT:
            {
                const uint8_t cell = static_cast<uint8_t>(zf_pop());
                mole::f_io->write(&cell, 1U);
            }
            break;
        case ZF_SYSCALL_PRINT:
            {
                // いちばん文字列が長いときで -2147483647
                char buf[11+1];
                const auto cell = zf_pop();
                std::snprintf(buf, sizeof(buf), ZF_CELL_FMT, cell);
                mole::f_io->write(buf);
            }
            break;
        case ZF_SYSCALL_TELL:
        	// @todo TELLが何の役にたつのか調べる
            {
                const zf_cell len  = zf_pop();
                const zf_cell addr = zf_pop();
                if (addr >= ZF_DICT_SIZE - len)
                {
                    zf_abort(ZF_ABORT_OUTSIDE_MEM);
                }
                const uint8_t* buf =
                    static_cast<uint8_t*>(zf_dump(nullptr)) + static_cast<int32_t>(addr);
                mole::f_io->write(buf, len);
            }
            break;
        default:
            mole::f_inst->userFunc(mole::f_io,
                static_cast<int32_t>(id - mole::f_userFuncBase), input);
            break;
    }

    return ZF_INPUT_INTERPRET;
}

void zf_host_trace(const char* fmt, va_list va)
{
    mole::f_io->write("\033[1;30m");
    char buf[32];
    std::vsnprintf(buf, sizeof(buf), fmt, va);
    if ('\n' == buf[0])
    {
        mole::f_io->write("\r");
    }
    mole::f_io->write(buf);
    mole::f_io->write("\033[0m");
}

}
