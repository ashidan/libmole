/**
 * @file
 * @brief 雑多なマクロや関数を集めたもの
 * @copyright 2024 molelord All Rights Reserved.
 * @see https://opensource.org/license/mit/
 */
#include "libmole/mole_misc.hpp"

namespace mole {

void flipArray(uint8_t* buf, size_t elements) noexcept
{
    for (uint32_t i = 0; i < (elements>>1); i++)
    {
        const uint8_t temp = buf[i];
        buf[i] = buf[elements-1-i];
        buf[elements-1-i] = temp;
    }
}

}
