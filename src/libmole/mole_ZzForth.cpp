/**
 * @file
 * @brief ユーザー関数で拡張したzForthを作る
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "libmole/mole_ZzForth.hpp"
#include "libmole/mole_trap.h" // mole_abort()
#include "zforth/zforth.h" // zf_init()
#include <cstdio> // snprintf()
#include <cstring> // strlen()

#include "FreeRTOS.h" // vPortMalloc()
#include "task.h" // uxTaskGetSystemState()

#define UF_SYSCALL_HELP        0
#define UF_SYSCALL_HEAPSPACE   1
#define UF_SYSCALL_SYSTEMSTATE 2

namespace mole {

/**
 * zForthのインスタンシエート時点で登録するワードの羅列。
 * 末尾の番兵を除き、各行を\0で終える必要がある。
 */
static const char* const f_words =
"help\0"
"heapspace\0"
"systemstate\0"
""; // 番兵

ZzForth::ZzForth(mole::ByteIo* io) noexcept : zForth(io, f_words)
{
}

static void word_help(mole::ByteIo* io)
{
    io->write(
"help\r\n"
"  pop x0 push x0\r\n"
"  ヘルプを表示する\r\n"
"heapspace\r\n"
"  pop x0 push x1\r\n"
"  FreeRTOS heapのxAvailableHeapSpaceInBytesをスタックへ積む\r\n"
"systemstate\r\n"
"  pop x0 push x0\r\n"
"  FreeRTOS uxTaskGetSystemState()の結果を表示する\r\n"
    );
}

void ZzForth::userFunc(mole::ByteIo* io, int32_t userFunc, const char* input) noexcept
{
    (void)input;
    MOLE_ASSERTNN(io);

    switch (userFunc)
    {
        case UF_SYSCALL_HELP:
        {
            word_help(io);
        }
        break;
        case UF_SYSCALL_HEAPSPACE:
        {
            xHeapStats stats;
            vPortGetHeapStats(&stats);
            zf_cell result = stats.xAvailableHeapSpaceInBytes;
            zf_push(result);
        }
        break;
        case UF_SYSCALL_SYSTEMSTATE:
        {
            const auto tasks = uxTaskGetNumberOfTasks();
            TaskStatus_t* stat = reinterpret_cast<TaskStatus_t*>(
                pvPortMalloc(sizeof(TaskStatus_t)*tasks));

            /* このワードはデバッグ用途であり、メモリを確保不能だったとしても
             * アボートすべきではないため、たんにbreakで抜ける。 */
            if (nullptr == stat) break;

            char buf[8+configMAX_TASK_NAME_LEN];
            snprintf(buf, sizeof(buf), " # %-*s",
                configMAX_TASK_NAME_LEN, "name");
            io->write(buf);
            io->write("Pri/Ba Sta StkSpc\r\n");

            const auto num = uxTaskGetSystemState(stat, tasks, NULL);
            for (uint32_t i = 0; i < num; i++)
            {
                snprintf(buf, sizeof(buf), "%2lu %-*s",
                    stat[i].xTaskNumber, configMAX_TASK_NAME_LEN, stat[i].pcTaskName);
                io->write(buf);

                snprintf(buf, sizeof(buf), " %2lu/%2lu ",
                    stat[i].uxCurrentPriority, stat[i].uxBasePriority);
                io->write(buf);

                static const char * const taskSt[] = {
                    "Rdy", "Run", "Blk", "Sus", "Del",
                };
                MOLE_ASSERT(stat[i].eCurrentState < eInvalid, stat[i].eCurrentState);
                io->write(taskSt[stat[i].eCurrentState]);

                snprintf(buf, sizeof(buf), " %6lu\r\n",
                    stat[i].usStackHighWaterMark * sizeof(StackType_t));
                io->write(buf);
            }
            vPortFree(stat);
        }
        break;
        default:
        {
            mole_abort(userFunc);
        }
        break;
    }
}

}
