/**
 * @file
 * @brief バイト入出力用のインタフェースクラス
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_BYTEIO_HPP
#define MOLE_BYTEIO_HPP

#include <cstdint> // uint8_t 
#include <cstddef> // size_t

#if !defined(MOLE_USE_UTEST)
#   include "FreeRTOS.h"
#   include "task.h" // TaskHandle_t
#else
typedef void* TaskHandle_t;
#endif

namespace mole {

/**
 * @brief インタフェース
 */
class ByteIo
{
public:
    /**
     * @brief コンストラクタ
     */
    ByteIo() noexcept {}

    /**
     * @brief デストラクタ
     */
    virtual ~ByteIo() noexcept {}

    /**
     * @brief データ列を送信する
     * @param[in] buf データ列 !=nullptr
     * @param[in] bytes bufのバイト数
     * @details
     * クラスが内部に持つ送信バッファへ、データ列を格納する。
     * bufの寿命は、このメソッドがリターンするまであれば足りる。
     * 内部の送信バッファに十分な空きがないときは、空きができるまで
     * ブロッキングする。
     */
    virtual void write(
        const uint8_t* buf,
        size_t bytes) noexcept = 0;

    /**
     * @brief データ列を送信する
     * @param[in] buf データ列 !=nullptr
     * @param[in] bytes bufのバイト数
     */
    virtual void write(
        const char* buf,
        size_t bytes) noexcept = 0;

    /**
     * @brief 0終端文字列を送信する
     * @param[in] buf 0終端文字列 !=nullptr
     */
    virtual void write(
        const char* buf) noexcept = 0;

    /**
     * @brief 現在までに受信できているデータ列を得る
     * @param[out] buf データ列の格納先 !=nullptr
     * @param[in] bytes bufのバイト数
     * @return 実際に受信できたバイト数
     * @details
     * 何も受信できていないときはブロッキングする。
     * このメソッドがリターンするときは、少なくとも戻り値は1以上である。
     * もし、受信エラーの発生があったならば、それが観測された位置の
     * 文字が(意図的に) 0xFF に化けている。
     */
    virtual size_t read(uint8_t* buf, size_t bytes) noexcept = 0;

    /**
     * @brief 現在までに受信できているデータ列を得る
     * @param[out] buf データ列の格納先 !=nullptr
     * @param[in] bytes bufのバイト数
     * @return 実際に受信できたバイト数
     * @details
     * read()のchar*型版。
     * @note 0終端文字列となるのは、受信したデータに0が含まれていた
     * 場合だけである。
     */
    virtual size_t read(char* buf, size_t bytes) noexcept = 0;

    /**
     * @brief 現在までに受信できているデータをひとつ得る 符号無し版
     * @return 受信したデータ
     * @details
     * 何も受信できていないときはブロッキングする。
     * このメソッドは std::runtime_error の派生クラスの例外をthrow
     * する可能性がある。
     */
    virtual uint8_t read() = 0;

    /**
     * @brief 現在までに受信できているデータをひとつ得る 符号あり版
     * @return 受信したデータ
     * @details
     * uint8_t read()の符号あり版。
     */
    virtual int8_t readc() = 0;

    /**
     * @brief read()をするタスクをインスタンスへ教える
     * @param[in] task read()をするタスクのハンドル !=nullptr
     * @details ByteIo派生クラスの実装では、read()のブロッキング解除のため
     * にFreeRTOSのTask Notifications機能を使用している。
     * そのため、ByteIoのインスタンスは「read()したがっている
     * タスクは誰なのか」を知る必要があり、このメソッドはそれを
     * インスタンスへ教えるために存在する。
     * なお、このメソッドが呼ばれていない場合はコンストラクタを
     * 呼んだタスクが「read()したがっているタスク」とみなされる。
     */
    virtual void setReaderTask(TaskHandle_t task) noexcept = 0;
};

}
#endif
