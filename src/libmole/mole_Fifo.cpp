/**
 * @file
 * @brief ソフトウェアで実装したFIFO
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#include "libmole/mole_Fifo.hpp"
#include "libmole/mole_trap.h" // MOLE_ASSERT()
#include <cstring> // strlen()

namespace mole {

Fifo::Fifo(
    uint8_t* ring,
    size_t bytes) noexcept : m_ring(ring), m_mask(bytes-1),
    m_wpos(0), m_rpos(0), m_prev_txcandidate(0)
{
    MOLE_ASSERTNN(ring);
    MOLE_ASSERT(bytes >= 2, bytes);
    MOLE_ASSERT(0 == (bytes & (bytes>>1)), bytes); // 2の階乗？
}

Fifo::Fifo(
    Buffer* ring) noexcept : m_ring(ring->m_buf), m_mask(ring->m_bytes-1),
    m_wpos(0), m_rpos(0), m_prev_txcandidate(0)
{
    MOLE_ASSERTNN(ring);
}

size_t Fifo::push(
    const uint8_t* buf,
    size_t bytes) noexcept
{
    MOLE_ASSERTNN(buf);

    const Status status(this);
#if 0
    size_t w = status.w;
    const size_t space = status.space;
    const size_t copiable = (bytes < space) ? bytes : space;

    for (size_t i = 0; i < copiable; i++)
    {
        m_ring[w] = buf[i];
        w = (w + 1) & m_mask;
    }
    m_wpos.store(w);
#else
    const size_t current = status.w;
    const size_t space = status.space;
    const size_t copiable = (bytes < space) ? bytes : space;
    if (0U == copiable) return 0U;

    const size_t finish = (current + copiable) & m_mask;
    if (current < finish)
    {
        std::memcpy(&m_ring[current], buf, copiable);
    }
    else // current > finish
    {
        const size_t firstHalf  = m_mask + 1U - current;
        const size_t secondHalf = copiable - firstHalf;
        std::memcpy(&m_ring[current], buf, firstHalf);
        if (secondHalf > 0U)
        {
            std::memcpy(&m_ring[0], &buf[firstHalf], secondHalf);
        }
    }
#if configNUMBER_OF_CORES >= 2
    m_wpos.store(finish);
#else
    m_wpos = finish;
#endif

#endif

    return copiable;
}

size_t Fifo::push(
    const char* buf,
    size_t bytes) noexcept
{
    return push(reinterpret_cast<const uint8_t*>(buf), bytes);
}

size_t Fifo::push(
    const char* buf) noexcept
{
    return push(reinterpret_cast<const uint8_t*>(buf), std::strlen(buf));
}

bool Fifo::isEnough() const noexcept
{
    const Status status(this);
    const size_t max = m_mask + 1U;
    const size_t threshold = (max>>1U) + (max>>2U);
    return (status.stock >= threshold) ? true : false;
}

size_t Fifo::getStock() const noexcept
{
    const Status status(this);
    return status.stock;
}

size_t Fifo::getSpace() const noexcept
{
    const Status status(this);
    return status.space;
}

size_t Fifo::pop(
    uint8_t* buf,
    size_t bytes) noexcept
{
    MOLE_ASSERTNN(buf);

    const Status status(this);
#if 0
    size_t r = status.r;
    const size_t stock = status.stock;
    const size_t copiable = (bytes < stock) ? bytes : stock;

    for (size_t i = 0; i < copiable; i++)
    {
        buf[i] = m_ring[r];
        r = (r + 1) & m_mask;
    }
#if configNUMBER_OF_CORES >= 2
    m_rpos.store(r);
#else
    m_rpos = r;
#endif
#else
    const size_t current = status.r;
    const size_t stock = status.stock;
    const size_t copiable = (bytes < stock) ? bytes : stock;
    if (0U == copiable) return 0U;

    const size_t finish = (current + copiable) & m_mask;
    if (current < finish)
    {
        std::memcpy(buf, &m_ring[current], copiable);
    }
    else // current > finish
    {
        const size_t firstHalf  = m_mask + 1U - current;
        const size_t secondHalf = copiable - firstHalf;
        std::memcpy(buf, &m_ring[current], firstHalf);
        if (secondHalf > 0U)
        {
            std::memcpy(&buf[firstHalf], &m_ring[0], secondHalf);
        }
    }
#if configNUMBER_OF_CORES >= 2
    m_rpos.store(finish);
#else
    m_rpos = finish;
#endif
#endif

    return copiable;
}

size_t Fifo::getDmaTxCandidatePart(uint8_t **addr) noexcept
{
    MOLE_ASSERTNN(addr);

    const Status status(this);
    const uint32_t w = status.w;
    const uint32_t r = status.r;

    if (w == r)
    {
        m_prev_txcandidate = 0U;
    }
    else
    {
        *addr = &m_ring[r];
        if (w > r)
        {
            m_prev_txcandidate = w - r;
        }
        else
        {
            const uint32_t last_index = m_mask;
            m_prev_txcandidate = last_index + 1U - r;
        }
    }
    return m_prev_txcandidate;
}

void Fifo::markPoped() noexcept
{
#if configNUMBER_OF_CORES >= 2
    const uint32_t r = m_rpos.load();
    m_rpos.store((r + m_prev_txcandidate) & m_mask);
#else
    const uint32_t r = *(const_cast<uint32_t*>(&m_rpos));
    m_rpos = (r + m_prev_txcandidate) & m_mask;
#endif
    m_prev_txcandidate = 0U;
}

}
