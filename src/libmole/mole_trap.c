/**
 * @file
 * @brief 致命的なエラーを捕える
 * @details ハードウェアブレークポイントは数に限りがあるので、
 * mole_abort()にひとつ設定するだけで何でも捕えられるようにする。
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "libmole/mole_trap.h"
#if !defined(MOLE_USE_UTEST)
#   include "FreeRTOS.h"
#   include "task.h" // taskDISABLE_INTERRUPTS()
#else
#   define taskDISABLE_INTERRUPTS()
#   define __BKPT(x) ((void)0)
#endif

void mole_abort(uintptr_t line)
{
    (void)line;
    taskDISABLE_INTERRUPTS();
#if defined(__arm__)
    __BKPT(0);
#endif
#if defined(__RX__)
    __builtin_rx_brk();
#endif
    for (;;) ; // ここにブレークポイントを設定する
}

void mole_assertx(uintptr_t var)
{
    mole_abort(var);
}

#if !defined(NDEBUG)
#   if defined(__arm__)
// 警告抑制のためプロトタイプ宣言をする
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void SecureFault_Handler(void);
void DebugMon_Handler(void);

void HardFault_Handler(void)   { mole_abort(0U); }
void MemManage_Handler(void)   { mole_abort(0U); }
void BusFault_Handler(void)    { mole_abort(0U); }
void UsageFault_Handler(void)  { mole_abort(0U); }
void SecureFault_Handler(void) { mole_abort(0U); }
void DebugMon_Handler(void)    { mole_abort(0U); }
#   endif // __arm__
#endif // !NDEBUG
