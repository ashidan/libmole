/**
 * @file
 * @brief CUI用のユーザー入力受付を提供する
 * @details
 * コマンド文字列の長さが端末画面の幅よりも長くなったとし
 * ても、入力行をスクロールさせることで画面表示の乱れを
 * 防ぐ機能を持つ。
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_SHELL_HPP
#define MOLE_SHELL_HPP

#include "libmole/mole_ByteIo.hpp" // ByteIo
#include "libmole/mole_Interpreter.hpp" // Interpreter

namespace mole {

/**
 * @brief CUI用のユーザー入力受付を提供する
 */
class Shell
{
private:
    ByteIo* const m_io;
    Interpreter* const m_interp;
    uint8_t* const m_buf;
    const size_t   m_bufBytes;
    const uint32_t m_windowWidth;
    const char     m_promptReady;
    const char     m_promptScroll;
    const char     m_promptFull;

public:
    /**
     * @brief カスタマイズ可能なほうのコンストラクタ
     * @param[inout] io 入出力に使うインスタンス
     * @param[inout] interp インタープリターインスタンス
     * @param[inout] buf ユーザー入力を格納する領域
     * @param[in] bufBytes bufのバイト数 >=4
     * @param[in] screenWidth 端末の1行に表示可能な文字数 >=4
     * @param[in] promptReady 入力受付可能を示す文字
     * @param[in] promptScroll スクロール中を示す文字
     * @param[in] promptReady 入力の満杯を示す文字
     * @note io、interp、およびbufの寿命は、Shellと同じかより長く
     * なければならない。
     */
    Shell(ByteIo* io, Interpreter* interp,
        uint8_t* buf, size_t bufBytes, uint32_t screenWidth,
        char promptReady, char promptScroll, char promptFull) noexcept;

    /**
     * @brief 一部がデフォルト設定になるコンストラクタ
     * @param[inout] io 入出力に使うインスタンス
     * @param[inout] interp インタープリターインスタンス
     * @param[inout] buf ユーザー入力を格納する領域
     * @param[in] bufBytes bufのバイト数
     * @note io、interp、およびbufの寿命は、Shellと同じかより長く
     * なければならない。
     */
    Shell(ByteIo* io, Interpreter* interp,
        uint8_t* buf, size_t bufBytes) noexcept;

    Shell(const Shell&) = delete;
    Shell& operator=(const Shell&) = delete;
    Shell(Shell&& rhs) = delete;
    Shell& operator=(Shell&& rhs) = delete;

    /**
     * @brief デストラクタ
     */
    virtual ~Shell() noexcept {}

    /**
     * @brief CUIを実行する
     * @note このメソッドはリターンしないので、実行したタスクは
     * CUI以外のことは何もできない。
     */
    [[noreturn]] void exec() noexcept;
};

}
#endif
