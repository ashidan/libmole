/**
 * @file
 * @brief libmoleに必須のマクロに関して、設定の上書きをする
 * @details
 * このファイルはapp/app_FreeRTOSConfig.hからincludeされる。
 * RXのコンフィグレーターが作るFreeRTOSConfig.hは、ユーザー側で
 * 設定値を上書きする自由度が無い。
 * 仕方がないので、#undefを使って強引に再定義する。
 * RAのコンフィグレーターが作るFreeRTOSConfig.hは、ユーザー側の
 * 設定値が存在するならばそれを尊重してくれるので、#undefの
 * 必要は無いが、#undefがあっても悪さはしない。
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_FREERTOSCONFIG_H
#define MOLE_FREERTOSCONFIG_H

#if defined(configASSERT)
#   undef configASSERT
#endif
#if defined(configTIMER_TASK_PRIORITY)
#   undef configTIMER_TASK_PRIORITY
#endif
#if defined(configAPPLICATION_ALLOCATED_HEAP)
#   undef APPLICATION_ALLOCATED_HEAP
#endif
#if defined(portCONFIGURE_TIMER_FOR_RUN_TIME_STATS)
#   undef portCONFIGURE_TIMER_FOR_RUN_TIME_STATS
#endif
#if defined(portGET_RUN_TIME_COUNTER_VALUE)
#   undef portGET_RUN_TIME_COUNTER_VALUE
#endif

#if defined(NDEBUG)
#   define configASSERT(x) ((void)0)
#else
#   include "mole_trap.h" // mole_assertx()
#   define configASSERT(x) if ((x) == 0) mole_assertx(__LINE__)
#endif

/**
 * configMAX_PRIORITIESがどんな値であろうとも、TIMER_TASKの優先度を
 * 最高にする。FSP ConfigurationでconfigMAX_PRIORITIESをいじる
 * たびにconfigTIMER_TASK_PRIORITYも一緒に数値入力しなおすのは
 * 面倒でかつ変更し忘れによるバグの元なので、自動調整にする。
 * なお、最高優先度にするのは、xTimerPendFunctionCallFromISR()を
 * 利用して、ISRでやり残した処理をタスクコンテキストで行うために
 * 都合が良いから。
 * RAのPCD_TSKも自動で最高優先度になるが、同一の優先度であれば
 * (Use Time SlicingをEnabledにしないかぎりは)お互いにプリエンプト
 * されないので、問題はないはず。
 */
#define configTIMER_TASK_PRIORITY (configMAX_PRIORITIES - 1)

//! mole_wrap.cでのrealloc()の動作のために必要
#define configAPPLICATION_ALLOCATED_HEAP 1

/* configGENERATE_RUN_TIME_STATSを1にしたとき必要になる、2つのマクロを
 * ここで定義する。
 * configGENERATE_RUN_TIME_STATSが1でない場合、RTOSリソースのStackタブに
 * EndOfStack、StackSize、StackUsageSizeが表示されず、デバッグに不便に
 * なる。
 * なお、このport〜STATS()とport〜VALUE()の実装はコンパイルエラーを防ぐ
 * だけの最小限のものなので、TaskタブのTotalTickCount、DeltaTickCountの
 * 表示は役に立たない。
 * configGENERATE_RUN_TIME_STATS自体は、プロジェクトのFSP Configuration
 * あるいはSmart Configurator上でEnableする必要がある。 */
#define portCONFIGURE_TIMER_FOR_RUN_TIME_STATS() ((void)0)
#define portGET_RUN_TIME_COUNTER_VALUE() (1)

/* RXマイコンだと、FreeRTOS.hから芋蔓式にincludeされるportmacro.hにある、
 * ファイルローカルな関数 set_interrupt_mask_from_isr() に関して未使用という
 * 警告が出るので、無視する。 */
#if defined(__RX__)
#   pragma GCC diagnostic ignored "-Wunused-function"
#endif

#endif
