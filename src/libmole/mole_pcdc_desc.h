/**
 * @file
 * @brief PCDC Descriptorを用意する
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_PCDC_DESC_H
#define MOLE_PCDC_DESC_H

#include <stdint.h> // uint16_t

#if defined(__cplusplus)
extern "C" {
#endif

/**
 * @brief USB PCDC Descriptor内のベンダーIDとプロダクトIDを変更する
 * @param[in] vendorId ベンダーID
 * @param[in] productId プロダクトID
 */
void mole_pcdc_setVidPid(uint16_t vendorId, uint16_t productId);

/**
 * @brief USB PCDC Descriptor内のiSerialNumberを変更する
 * @param[in] number シリアル番号 1<=x<=9
 * @note 目的は量産ではなく、1台のPCに複数の評価ボードを接続するときに
 * シリアル番号の衝突を避けることなので、単純化のため値域が限られている。
 */
void mole_pcdc_setSerialNumber(uint32_t number);

#if defined(__cplusplus)
}
#endif

#endif
