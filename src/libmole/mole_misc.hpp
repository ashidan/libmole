/**
 * @file
 * @brief 雑多なマクロや関数を集めたもの
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_MISC_HPP
#define MOLE_MISC_HPP

#include <cstdint> // uintptr_t
#include <cstddef> // size_t

#define MOLE_ESC_GREEN   "\033[32m"
#define MOLE_ESC_RED     "\033[31m"
#define MOLE_ESC_DEFAULT "\033[39m"

namespace mole {

/**
 * @brief 配列の要素数を返す
 * @param[in] N 対象の配列
 * @return 配列の要素数
 * @details Effective Modern C++ 日本語版 初版 p.9を参考にした
 */
template<typename T, std::size_t N>
constexpr std::size_t elementsOf(T (&)[N]) noexcept
{
    return N;
};

/**
 * @brief 配列の並びをひっくり返す
 * @param[inout] buf 対象の配列
 * @param[in] elements 要素数 
 */
void flipArray(uint8_t* buf, size_t elements) noexcept;

}
#endif
