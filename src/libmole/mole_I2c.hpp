/**
 * @file
 * @brief i2cマスター通信をする
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_I2C_HPP
#define MOLE_I2C_HPP

#if defined(__RX__)
extern "C"
{
#include "r_smc_entry.h" // riic_info_t
#include "r_riic_rx_if.h" // riic_info_t
}
#endif

namespace mole {

class I2c
{
private:
#if defined(__RX__)
    riic_info_t m_info;
    uint8_t m_send[16]; // 16バイトあればまず足りるはず
    uint8_t m_slvaddr;
#endif
public:

    /**
     * @brief コンストラクタ
     */
    I2c(uint8_t ch_no) noexcept;

    I2c(const I2c&) = delete;
    I2c& operator=(const I2c&) = delete;
    I2c(I2c&& rhs) = delete;
    I2c& operator=(I2c&& rhs) = delete;

    /**
     * @brief 送信する
     * @param[in] slvaddr スレーブアドレス
     * @param[in] s1 送信データ1 !=nullptr
     * @param[in] s1bytes 送信データ1の数 >=1
     * @param[in] s2 送信データ1 !=nullptr
     * @param[in] s2bytes 送信データ2の数 >=1
     */
    void send(uint8_t slvaddr,
        const uint8_t* s1, size_t s1bytes,
        const uint8_t* s2, size_t s2bytes) noexcept;

    /**
     * @brief 送信する
     * @param[in] slvaddr スレーブアドレス
     * @param[in] s 送信データ !=nullptr
     * @param[in] sbytes 送信データ数 >=1
     */
    void send(uint8_t slvaddr,
        const uint8_t* s, size_t sbytes) noexcept;

    /**
     * @brief スレーブアドレスのみ送信する
     * @param[in] slvaddr スレーブアドレス
     */
    void send(uint8_t slvaddr) noexcept;

    /**
     * @brief スタートとストップのみ送信する
     */
    void startAndStop() noexcept;

    /**
     * @brief スレーブアドレス送信に続いて受信する
     * @param[in] slvaddr スレーブアドレス
     * @param[out] r 受信データ格納先 !=nullptr
     * @param[in] rbytes 受信データ数 >=1
     * @return true:成功
     */
    bool recv(uint8_t slvaddr,
        uint8_t* r, size_t rbytes) noexcept;

    /**
     * @brief スレーブアドレス・データ送信に続いて受信する
     * @param[in] slvaddr スレーブアドレス
     * @param[in] s 送信データ !=nullptr
     * @param[in] sbytes 送信データ数 >=1
     * @param[out] r 受信データ格納先 !=nullptr
     * @param[in] rbytes 受信データ数 >=1
     * @return true:成功
     */
    bool sendAndRecv(uint8_t slvaddr,
        const uint8_t* s, size_t sbytes,
        uint8_t* r, size_t rbytes) noexcept;

private:
#if defined(__RX__)
#   if RIIC_CFG_CH0_INCLUDED == 1
    static I2c* inst0;
    static void callback0(void) noexcept;
#   endif
#   if RIIC_CFG_CH1_INCLUDED == 1
    static I2c* inst1;
    static void callback1(void) noexcept;
#   endif
#   if RIIC_CFG_CH2_INCLUDED == 1
    static I2c* inst2;
    static void callback2(void) noexcept;
#   endif
#endif
};

}

#endif
