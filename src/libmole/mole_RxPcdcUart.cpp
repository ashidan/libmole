/**
 * @file
 * @brief PCDC UARTをラップするクラス
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#if defined(__RX__)
#include "libmole/mole_config.h" // MOLE_CONFIG_PCDCUART
#if (MOLE_CONFIG_PCDCUART > 0U)

extern "C" {
#   include "r_usb_basic_if.h" // r_usb_pcdc_if.hのインクルードに必要
#   include "r_usb_pcdc_if.h" // R_USB_Open()
}

#include "libmole/mole_PcdcUart.hpp" // PcdcUart
#include "libmole/mole_trap.h" // MOLE_ALWAYS()
#include "libmole/mole_misc.hpp" // elementsOf()
#include "libmole/mole_Fifo.hpp" // Fifo

#include "FreeRTOS.h"
#include "task.h" // vTaskSuspendAll()
#include "message_buffer.h" // MessageBufferHandle_t

#include <atomic> // atomic_uint
#include <new> // new(std::nothrow)
#include <cstring> // strlen()

/* usb_rtos_unconfiguration() を呼ぶためにr_usb_cstd_rtos.h
 * をincludeしたいが、r_usb_cstd_rtos.hが依存するヘッダ r_usb_extern.h
 * に class という名前の仮引数があり、C++ではコンパイル不能である。
 * 仕方がないので、プロトタイプ宣言を書くことにする。 */
extern "C" rtos_err_t usb_rtos_unconfiguration(void);

// RA向けに書いたソースをRXに流用するため、別名を作る
#define FSP_SUCCESS                 USB_SUCCESS              // @r_usb_basic_if.h
#define USB_STATUS_READ_COMPLETE    USB_STS_READ_COMPLETE    // @r_usb_basic_if.h
#define USB_STATUS_WRITE_COMPLETE   USB_STS_WRITE_COMPLETE   // @r_usb_basic_if.h
#define USB_STATUS_CONFIGURED       USB_STS_CONFIGURED       // @r_usb_basic_if.h
#define USB_STATUS_REQUEST          USB_STS_REQUEST          // @r_usb_basic_if.h
#define USB_STATUS_REQUEST_COMPLETE USB_STS_REQUEST_COMPLETE // @r_usb_basic_if.h
#define USB_STATUS_DETACH           USB_STS_DETACH           // @r_usb_basic_if.h
#define USB_STATUS_SUSPEND          USB_STS_SUSPEND          // @r_usb_basic_if.h
#define USB_STATUS_RESUME           USB_STS_RESUME           // @r_usb_basic_if.h

/* RXではRAとは異なり、「callback関数の中でR_USB_Read()やWrite()を呼ぶと
 * 正しく動作しない」ようだ。
 * r01an2238xx0140_usb の RX64M向けサンプルプログラムでは、callback関数
 * (PCD_TSKコンテキスト)の中からmailboxを介してMAIN_TSKへusb_ctrl_t型
 * オブジェクトを渡し、MAIN_TSKでイベント処理するという方式で動いている。
 * このRx版実装は上記サンプルに似せて作っているが、MAIN_TASKでなく独自の
 * UsbDaemonThr を作ってイベント処理することにした。 */

namespace mole {

/**
 * @brief PCDC UARTをラップするクラス
 * @details プラットフォームのPCDC UART APIを包んで、容易に使えるようにする。
 * 複数のタスクから1つのPCDC UARTへ送信することも考慮して、排他には
 * task notificationでなくSemaphoreを使用している。
 */
class PcdcUart : public ByteIo {
    static std::atomic_uint instances;

    usb_ctrl_t            m_usb;
    Fifo                  m_txFifo;
    Fifo                  m_rxFifo;
    TaskHandle_t          m_taskToNotify;
    uint8_t               m_rxBuf[64]; //!< R_USB_Read()で使う領域
    SemaphoreHandle_t     m_writeSem;
    MessageBufferHandle_t m_ctrlQ;
    bool                  m_attached;
    UBaseType_t           m_indexToNotify;

    StaticSemaphore_t     m_writeSemEntity;
    StackType_t           m_stack[512/sizeof(StackType_t)];
    StaticTask_t          m_tcb;
    StaticMessageBuffer_t m_ctrlQEntity;
    uint8_t               m_ctrlQStorage[8*(sizeof(size_t)+sizeof(void*))];

public:
    PcdcUart(const PcdcUart&) = delete;
    PcdcUart& operator=(const PcdcUart&) = delete;
    PcdcUart(PcdcUart&& rhs) = delete;
    PcdcUart& operator=(PcdcUart&& rhs) = delete;

    /**
     * @brief コンストラクタ
     * @param[in] usb FITのUSBインスタンス
     * @param[in] cfg R_USB_Open()に与えるコンフィグレーション
     * @param[inout] txRing 送信リングバッファとして使う領域 !=nullptr
     * @param[inout] rxRing 受信リングバッファとして使う領域 !=nullptr
     * @param[in] priority UsbDaemonThrの優先度
     * @param[in] indexToNotify Notifyに使うインデックス < configTASK_NOTIFICATION_ARRAY_ENTRIES
     * @note txRingおよびrxRingの寿命は、PcdcUartインスタンスと同じかより長い
     * 必要がある。もしringのほうが寿命が短い場合、不正な読み書きを引き
     * 起こす可能性がある。
     */
    PcdcUart(
        const usb_ctrl_t* usb, const usb_cfg_t* cfg,
        Buffer* txRing, Buffer* rxRing,
        uint32_t priority,
        uint32_t indexToNotify) noexcept;

    virtual ~PcdcUart() noexcept {}

    virtual void write(
        const uint8_t* buf,
        size_t bytes) noexcept override;

    virtual void write(
        const char* buf,
        size_t bytes) noexcept override;

    virtual void write(
        const char* buf) noexcept override;

    virtual size_t read(uint8_t* buf, size_t bytes) noexcept override;

    virtual size_t read(char* buf, size_t bytes) noexcept override;

    virtual uint8_t read() override;

    virtual int8_t readc() override;

    virtual void setReaderTask(TaskHandle_t task) noexcept override;

private:
    static void *operator new(size_t bytes, const std::nothrow_t&) noexcept
    {
        (void)bytes;
        static uintptr_t ram[MOLE_CONFIG_PCDCUART * sizeof(PcdcUart)/sizeof(uintptr_t)];

        const uint32_t myIndex = instances.fetch_add(1U);
        MOLE_ALWAYS(myIndex < MOLE_CONFIG_PCDCUART, myIndex);
        return &ram[myIndex * sizeof(PcdcUart)/sizeof(uintptr_t)];
    }

    /**
     * @brief newと対になる、nothrow用のdelete
     * @param ptr
     * @details new と対になる delete。デストラクタは暗黙では呼び出されない。
     */
    static void operator delete(void* ptr, const std::nothrow_t&) noexcept
    {
#if defined(MOLE_USE_UTEST)
        /* 必要であれば、ここでデストラクタに相当する処理を行う。
         * PcdcUart* const p = static_cast<PcdcUart*>(ptr);
         */

        (void)ptr;
        const uint32_t count = instances.fetch_sub(1U);
        MOLE_ALWAYS(count >= 1, count);
#else
        (void)ptr;
#endif
    }

    /**
     * @brief nothrow用deleteを用意した場合、デストラクタから呼ばれるふつうのdeleteも用意する必要がある
     * @param ptr 無視される
     */
    static void operator delete(void* ptr) noexcept
    {
        (void)ptr;
    }

    static void callback(
        usb_ctrl_t* info, rtos_task_id_t cur_task,
        uint8_t usb_state) noexcept;
    static void usb_daemon_tsk(void* pvParameters) noexcept;

public:
    static PcdcUart* create(const usb_ctrl_t* usb, const usb_cfg_t* cfg,
        Buffer* txRing, Buffer* rxRing,
        uint32_t priority,
        uint32_t indexToNotify) noexcept
    {
        return new(std::nothrow) PcdcUart(usb, cfg,
            txRing, rxRing,
            priority,
            indexToNotify);
    }
#if defined(MOLE_USE_UTEST)
    static void destroy(ByteIo* uart) noexcept
    {
        Uart::operator delete(uart, std::nothrow);
    }
#endif
};

std::atomic_uint PcdcUart::instances;

/**
 * コールバック関数のなかで使用する、PcdcUartのインスタンスへのポインタ
 * ユーザーの指定した情報をコールバック関数へ渡す方法が無く、かつ、
 * PcdcUartインスタンスは複数生成されることはないと決め打ちして、
 * 静的に持つことにする。
 */
static PcdcUart* f_PcdcUartInstance;

/* 静的メンバーメソッド
 * このコールバック関数は、割り込みコンテキストではなくPCD_TSKの中で呼ばれる。
 */
void PcdcUart::callback(
    usb_ctrl_t* ctrl, rtos_task_id_t cur_task,
    uint8_t usb_state) noexcept
{
    (void)cur_task;
    (void)usb_state;

    // ここでキューに押し込むのはポインターだけ。
    const auto bytes = xMessageBufferSend(
        f_PcdcUartInstance->m_ctrlQ, &ctrl, sizeof(void*), 0);
    MOLE_ALWAYS(sizeof(void*) == bytes, bytes);
}

/* 静的メンバーメソッド */
void PcdcUart::usb_daemon_tsk(void *pvParameters) noexcept
{
    union LineCoding {
        usb_pcdc_linecoding_t s;
        uint8_t d[7];
    };
    static LineCoding linecoding;

    PcdcUart* const inst = static_cast<PcdcUart*>(pvParameters);

    for (;;)
    {
        usb_ctrl_t ctrl;
        {
            const usb_ctrl_t* p;
            const auto bytes = xMessageBufferReceive(inst->m_ctrlQ, &p, sizeof(void*), portMAX_DELAY);
            MOLE_ALWAYS(sizeof(void*) == bytes, bytes);
            /* r01an2238xx0140_usb の r_usb_pcdc_echo_apl.c usb_main()に倣い、
             * ポインターでなく中身を丸ごとコピーする。 */
            ctrl = *p;
        }

        switch (ctrl.event)
        {
            case USB_STATUS_READ_COMPLETE:
            {
                /* ここに到達した時点で、R_USB_Read()へ与えたバッファへ
                 * 受信データが格納済み。*/
                inst->m_rxFifo.push(inst->m_rxBuf, ctrl.size);

                // 受信待ちをしているタスクに対し、データの到着を伝える
                const auto rc = xTaskNotifyIndexed(
                    inst->m_taskToNotify, inst->m_indexToNotify, 0U, eNoAction);
                MOLE_ALWAYS(pdTRUE == rc, rc);

                // 次の受信待ちを仕掛ける
                const auto err = R_USB_Read(&ctrl,
                    inst->m_rxBuf,
                    sizeof(inst->m_rxBuf));
                MOLE_ALWAYS(FSP_SUCCESS == err, err);
            }
            break;
            case USB_STATUS_WRITE_COMPLETE:
            {
                // 前回のwriteが終わったので、m_txFifoにそれを伝える
                inst->m_txFifo.markPoped();

                const size_t bytes = inst->m_txFifo.getStock();
                if (0U == bytes)
                {
                    const auto rc = xSemaphoreGive(inst->m_writeSem);
                    MOLE_ALWAYS(pdTRUE == rc, rc);
                }
                else
                {
                    uint8_t *addr = nullptr;
                    const size_t writable = inst->m_txFifo.getDmaTxCandidatePart(&addr);
                    /* xSemaphoreTake()がリターンする前に、callback()
                     * のなかでデータ送出を終えている可能性があり、
                     * writable==0がありうる */
                    if (writable > 0U)
                    {
                        const auto err = R_USB_Write(
                            &ctrl, addr, writable);
                        MOLE_ALWAYS(FSP_SUCCESS == err, err);
                    }
                    else
                    {
                        const auto rc = xSemaphoreGive(inst->m_writeSem);
                        MOLE_ALWAYS(pdTRUE == rc, rc);
                    }
                }
                break;
            }
            break;
            case USB_STATUS_CONFIGURED:
            {
                // 初回の受信待ちを仕掛ける
                ctrl.type = USB_PCDC;
                const auto err = R_USB_Read(&ctrl,
                    inst->m_rxBuf,
                    sizeof(inst->m_rxBuf));
                MOLE_ALWAYS(FSP_SUCCESS == err, err);

                /* この時点でWriteが可能なので、m_txFifoを調べて
                 * ホストに送るべきデータがあるなら送信を開始、
                 * 無いならセマフォを立てる。*/
                uint8_t *addr = nullptr;
                const size_t writable = inst->m_txFifo.getDmaTxCandidatePart(&addr);
                if (writable > 0U)
                {
                    const auto err = R_USB_Write(
                        &ctrl, addr, writable);
                    MOLE_ALWAYS(FSP_SUCCESS == err, err);
                }
                else
                {
                    const auto rc = xSemaphoreGive(inst->m_writeSem);
                    MOLE_ALWAYS(pdTRUE == rc, rc);
                }
            }
            break;
            case USB_STATUS_REQUEST:
            {
                switch (ctrl.setup.type & USB_BREQUEST)
                {
                    case USB_PCDC_SET_LINE_CODING:
                    {
                        ctrl.type   = USB_REQUEST;
                        //ctrl.module = USB_IP0;
                        // ホストから設定を貰って、その結果をlinecodingへ保存
                        const auto err = R_USB_Read(
                            &ctrl,
                            linecoding.d, sizeof(linecoding.d));
                        MOLE_ALWAYS(FSP_SUCCESS == err, err);
                    }
                    break;
                    case USB_PCDC_GET_LINE_CODING:
                    {
                        ctrl.type   = USB_REQUEST;
                        //ctrl.module = USB_IP0;
                        // 現在のlinecodingを、ホストへ渡す
                        const auto err = R_USB_Write(
                            &ctrl,
                            linecoding.d, sizeof(linecoding.d));
                        MOLE_ALWAYS(FSP_SUCCESS == err, err);
                    }
                    break;
                    case USB_PCDC_SET_CONTROL_LINE_STATE:
                    {
                        // 本来はここでRTS信号を制御する？
                        // 現在の状態を、ホストへ渡す
                        ctrl.type   = USB_REQUEST;
                        ctrl.status = USB_ACK;
                        const auto err = R_USB_Write(
                            &ctrl,
                            nullptr, 0U);
                        MOLE_ALWAYS(FSP_SUCCESS == err, err);
                    }
                    break;
                    default:
                    {
                        ctrl.type   = USB_REQUEST;
                        ctrl.status = USB_ACK;
                        const auto err = R_USB_Write(
                            &ctrl,
                            nullptr, 0U);
                        MOLE_ALWAYS(FSP_SUCCESS == err, err);
                    }
                    break;
                }
            }
            break;
            case USB_STATUS_REQUEST_COMPLETE:
            {
            }
            break;
            case USB_STATUS_DETACH: // ↓
            case USB_STATUS_SUSPEND:
            {
                inst->m_attached = false;
            }
            break;
            case USB_STATUS_RESUME:
            {
                inst->m_attached = true;
            }
            break;
            default:
            break;
        }
    }
}

PcdcUart::PcdcUart(const usb_ctrl_t* usb, const usb_cfg_t* cfg,
    Buffer* txRing, Buffer* rxRing,
    uint32_t priority,
    uint32_t indexToNotify) noexcept :
    m_usb(*usb), // 丸ごとコピー
    m_txFifo(txRing), m_rxFifo(rxRing),
    m_taskToNotify(xTaskGetCurrentTaskHandle()),
    m_rxBuf(),
    m_writeSem(nullptr),
    m_ctrlQ(nullptr),
    m_attached(false),
    m_indexToNotify(indexToNotify),
    m_writeSemEntity(),
    m_stack(), m_tcb(),
    m_ctrlQEntity(), m_ctrlQStorage()
{
    MOLE_ASSERTNN(usb);
    MOLE_ASSERTNN(cfg);
    // ringとbytesの検査はFifoコンストラクタで行うので、ここでは行わない
    MOLE_ASSERT(indexToNotify < configTASK_NOTIFICATION_ARRAY_ENTRIES, indexToNotify);

    m_writeSem = xSemaphoreCreateBinaryStatic(&m_writeSemEntity);
    MOLE_ALWAYS(nullptr != m_writeSem, m_writeSem);

    m_ctrlQ = xMessageBufferCreateStatic(sizeof(m_ctrlQStorage),
        m_ctrlQStorage, &m_ctrlQEntity);
    MOLE_ALWAYSNN(m_ctrlQ);

    const auto handle = xTaskCreateStatic(
        usb_daemon_tsk,
        "UsbDaemonTsk",
        mole::elementsOf(m_stack),
        this,
        priority,
        m_stack,
        &m_tcb);
    MOLE_ALWAYS(nullptr != handle, handle);

    /* Prosessing_Before_Start_Kernel() @ freertos_start.c が
     * usb_rtos_configuration()を呼んでしまうせいで、ふつうに
     * R_USB_Open()を呼ぶとPCD_TSKが2つ作られてしまう。
     * 問題の回避のため、usb_rtos_unconfiguration()を呼ぶことで
     * 初回のusb_rtos_configuration()を無かったことにする。 */
    const auto unconf = usb_rtos_unconfiguration();
    MOLE_ALWAYS(RTOS_SUCCESS == unconf, unconf);

    auto variable_cfg = *cfg; // R_USB_Open()は定数を受け取れない
    
    /* cfgのほうにはコールバック関数を書いていないので、
     * open()したあとにcallbackSet()の順で呼び出さないと
     * open()のせいでそれ以前のcallbackSet()が無効化されてしまう。 */
    const auto err = R_USB_Open(&m_usb, &variable_cfg);
    MOLE_ALWAYS(FSP_SUCCESS == err, err);

    // コールバック呼び出しに備え、ポインタを設定しておく
    f_PcdcUartInstance = this;

    R_USB_Callback(callback);
}

void PcdcUart::write(
    const uint8_t* buf,
    size_t bytes) noexcept
{
    MOLE_ASSERTNN(buf);
    if (0U == bytes) return;

    for (;;)
    {
        const size_t actual = m_txFifo.push(buf, bytes);

        if (pdTRUE == xSemaphoreTake(m_writeSem, 0U))
        {
            /* セマフォを獲得できたということは転送は未起動なので、
             * このタイミングで起動する。 */
            uint8_t *addr = nullptr;
            const size_t writable = m_txFifo.getDmaTxCandidatePart(&addr);
            /* xSemaphoreTake()がリターンする前に、usb_daemon_tsk()のなかで
             * データ送出を終えている可能性があり、writable==0がありうる */
            if (writable > 0U)
            {
                const auto err = R_USB_Write(
                    &m_usb, addr, writable);
                MOLE_ALWAYS(FSP_SUCCESS == err, err);
            }
            else
            {
                const auto rc = xSemaphoreGive(m_writeSem);
                MOLE_ALWAYS(pdTRUE == rc, rc);
            }
        }
        else
        {
            /* セマフォを獲得できなかったということは今は転送中であり、
             * 放っておけばcallbackが転送を再起動してくれるので何もしない。*/
        }

        if (actual >= bytes)
        {
            return;
        }

        // fifoの空きが十分でなく、押し込みきれなかったとき
        buf   += actual;
        bytes -= actual;

        /* 少し待って、push()に再挑戦する。
         * 本当は時間でなくイベントを待ちたいのであるが、適当な
         * ものがないので時間にしている。 */
        vTaskDelay(pdMS_TO_TICKS(1));
    }
}

void PcdcUart::write(
    const char* buf,
    size_t bytes) noexcept
{
    write(reinterpret_cast<const uint8_t*>(buf), bytes);
}

void PcdcUart::write(
    const char* buf) noexcept
{
    write(reinterpret_cast<const uint8_t*>(buf), std::strlen(buf));
}

size_t PcdcUart::read(uint8_t* buf, size_t bytes) noexcept
{
    size_t actual = 0U;
    for (;;)
    {
        actual = m_rxFifo.pop(buf, bytes);
        if (actual > 0)
        {
            break;
        }

        // 通知が来るまで寝て待つ
        ulTaskNotifyTakeIndexed(m_indexToNotify, pdTRUE, portMAX_DELAY);
    }
    return actual;
}

size_t PcdcUart::read(char* buf, size_t bytes) noexcept
{
    return read(reinterpret_cast<uint8_t*>(buf), bytes);
}

uint8_t PcdcUart::read()
{
    uint8_t data;
    read(&data, 1U);
    return data;
}

int8_t PcdcUart::readc()
{
    uint8_t data;
    read(&data, 1U);
    return static_cast<int8_t>(data);
}

void PcdcUart::setReaderTask(TaskHandle_t task) noexcept
{
    MOLE_ASSERTNN(task);
    m_taskToNotify = task;
}


namespace PcdcUartFactory {

    ByteIo* create(
        const usb_ctrl_t* usb, const usb_cfg_t* cfg,
        Buffer* txRing, Buffer* rxRing,
        uint32_t priority,
        uint32_t indexToNotify) noexcept
    {
        return PcdcUart::create(usb, cfg,
            txRing, rxRing,
            priority,
            indexToNotify);
    }

#if defined(MOLE_USE_UTEST)
    void destroy(ByteIo *uart) noexcept
    {
        Uart::destroy(uart);
    }
#endif
} // end of PcdcUartFactory

} // end of mole

#endif // MOLE_CONFIG_PCDCUART > 0U
#endif // __RX__
