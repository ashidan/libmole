/**
 * @file
 * @brief app/app_mole_config.hの機能指定を解釈するマクロを定義する
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 * @details
 * 各プロジェクトの src/app/app_mole_config.h において、 MOLE_CONFIGn
 * に対して、生成可能インスタンス数の上限を8進数で指定する。
 * 0を指定すると機能が無効化され、1〜7(最上位桁のみ1〜3)を指定すると
 * その数を上限としてインスタンスを作れるようになる。
 * なお、上限より実際に生成したインスタンスの数が少ない場合は
 * そのぶんRAMが無駄になるが、悪さはしない(つまり、RAMの無駄な消費を
 * 許容するならば、上限と生成数を完全に一致させる必要はない)。
 */
#ifndef MOLE_CONFIG_H
#define MOLE_CONFIG_H

#include "app/app_mole_config.h" // MOLE_CONFIG?

//                      ? -+ +- ?
//                    ? -+ | | +- ?
//                  ? -+ | | | | +- UART
//                     | | | | | |
//      MOLE_CONFIG1  011111111111
//                      | | | | |
//                   ? -+ | | | +- USB PCDC UART
//                     ? -+ | +- ?
//                          +- ?

#define MOLE_CONFIG_UART          ((MOLE_CONFIG1>>0U)&7U)
#define MOLE_CONFIG_PCDCUART      ((MOLE_CONFIG1>>3U)&7U)

#endif
