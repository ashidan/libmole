/**
 * @file
 * @brief C++オブジェクトファイルを含むリンクを成功させる
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

extern "C" {

// 警告の発生を防ぐため、プロトタイプを羅列する
#if defined(__RX__)
void kill(int pid, int sig) noexcept;
int getpid(void) noexcept;
#else
void _exit(int status) noexcept;
void _kill(int pid, int sig) noexcept;
int _getpid(void) noexcept;
#endif

#if defined(__RX__)
#else
void _exit(int status) noexcept
{
    (void)status;
    for (;;)
        ;
}
#endif

#if defined(__RX__)
void kill(int pid, int sig) noexcept
#else
void _kill(int pid, int sig) noexcept
#endif
{
    (void)pid;
    (void)sig;
    for (;;)
        ;
}

#if defined(__RX__)
int getpid(void) noexcept
#else
int _getpid(void) noexcept
#endif
{
    return 0;
}

}
