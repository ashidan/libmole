/**
 * @file
 * @brief 致命的なエラーを捕える
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_TRAP_H
#define MOLE_TRAP_H

#if defined(__cplusplus)
#   include <cstdint> // int32_t
extern "C" {
#else
#   include <stdint.h> // int32_t
#   include <stddef.h> // NULL
#endif

#if defined(NDEBUG)
// 未参照の変数に関する警告を抑止するため(void)varとする
#   define MOLE_ASSERT(cond, var) ((void)(var))
#   define MOLE_ASSERTNN(var) ((void)(var))
#else
    // とにかく何でも型変換したいのでCスタイルキャストを使う
#   define MOLE_ASSERT(cond, var) if (!(cond)) mole_assertx((uintptr_t)var)
#   if defined(__cplusplus)
#       define MOLE_ASSERTNN(var) if (nullptr == var) mole_assertx(0U)
#   else
#       define MOLE_ASSERTNN(var) if (NULL == var) mole_assertx(0U)
#   endif
#endif // NDEBUG

//! 役割はMOLE_ASSERT()と同じであるが、NDEBUGが定義されていても常に検査を行うマクロ
#define MOLE_ALWAYS(cond, var) if (!(cond)) mole_assertx((uintptr_t)var)
#if defined(__cplusplus)
#    define MOLE_ALWAYSNN(var) if (nullptr == var) mole_assertx(0U)
#else
#    define MOLE_ALWAYSNN(var) if (NULL == var) mole_assertx(0U)
#endif

/**
 * @brief 致命的なエラーを捕える
 * @param[in] line 行番号 デバッグの手掛かり
 * @details
 * 回復不能なエラーが発生したときに呼ぶ関数。
 * リリース版ではソフトウェアリセットを行う(未実装)。
 */
void mole_abort(uintptr_t line);

/**
 * @brief 検査に失敗したときに呼び出される関数
 * @param[in] var 発火したあとで参照したい変数
 * @details
 * __FILE__や__LINE__でなく変数を渡すのは、発火の元となった変数の値を、
 * 最適化が有効であっても mole_assertx() に飛んできたあとで観察できるように
 * するのが目的である。
 */
void mole_assertx(uintptr_t var);

#if defined(__cplusplus)
}
#endif

#endif
