/**
 * @file
 * @brief コールバック関数から、ユーザー関数を引数付きで呼び出す
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 * @details
 * RX用のライブラリは、コールバック関数の型が void func(void)
 * であることが多い。そのため、コールバックの延長上で何らかの
 * インスタンスのメソッドを呼びたいときなどに、インスタンスの
 * 特定に困ることがある。
 * そこで、実行時に「特定の値を引数として特定の番地へジャンプ
 * する処理」を機械語で配列へ書いて、それを中継地点として
 * ユーザー関数を呼び出すことにした。
 * @note いくつかのCPUアーキテクチャに備わっている、データ
 * 領域に置かれた命令コードの実行を許さないセキュリティ機能
 * と一緒に使う場合は、セキュリティ機能を迂回する必要がある
 * かもしれない。
 */
#ifndef MOLE_TRAMPOLINE_HPP
#define MOLE_TRAMPOLINE_HPP

#include <cstdint> // uint16_t

namespace mole {

/**
 * @brief コールバックの中継点を用意する
 */
class Trampoline final
{
private:
    const uint16_t m_code[(6+6+2)/sizeof(uint16_t)];
public:
    typedef void (*vfunc_v)(void) noexcept;
    typedef void (*vfunc_vp)(void*) noexcept;

    /**
     * @brief コンストラクタ
     * @param[in] func ユーザーの用意した引数付き関数 !=nullptr
     * @param[in] data 引数 !=nullptr
     */
    Trampoline(vfunc_vp func, void* data) noexcept;

    Trampoline(const Trampoline&) = delete;
    Trampoline& operator=(const Trampoline&) = delete;
    Trampoline(Trampoline&& rhs) = delete;
    Trampoline& operator=(Trampoline&& rhs) = delete;

    /**
     * @brief デストラクタ
     * @note 継承を禁止しているため、非virtualでよい。
     */
    ~Trampoline() noexcept {}

    /**
     * @brief 中継地点を返す
     * @return コールバック関数へのポインタ
     */
    vfunc_v getCb(void) noexcept;
};

}
#endif
