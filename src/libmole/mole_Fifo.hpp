/**
 * @file
 * @brief ソフトウェアでFIFOを実装する
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_FIFO_HPP
#define MOLE_FIFO_HPP

#include "libmole/mole_Buffer.hpp" // Buffer
#include <cstdint> // uint8_t 
#include <cstddef> // size_t

#if configNUMBER_OF_CORES >= 2
#include <atomic> // template atomic
#endif

namespace mole {

/**
 * @brief ソフトウェアで実現したFIFO
 * @details
 * 実装はリングバッファになっている。
 * 書き手はm_rposを改変しない、かつ、読み手はm_wposを改変しない、
 * というルールを前提として、ロックをせずにリングバッファの中身を操作している。
 * 実装にあたり、 ロックフリーアルゴリズムによるFIFOバッファ
 * https://qiita.com/Ryuz/items/f105a3e40df6f14e337a を参考にした。
 * @note ISRとタスクとの間は、現状の実装でいまのところ問題なく通信できている。
 * タスク同士の間でどうかはテストされていない。
 */
class Fifo
{
private:
    uint8_t* const m_ring;
    const uint32_t m_mask;
#if configNUMBER_OF_CORES >= 2
    std::atomic<uint32_t> m_wpos;
    std::atomic<uint32_t> m_rpos;
#else
    uint32_t m_wpos;
    uint32_t m_rpos;
#endif
    uint32_t m_prev_txcandidate;

public:
    Fifo(const Fifo&) = delete;
    Fifo& operator=(const Fifo&) = delete;
    Fifo(Fifo&& rhs) = delete;
    Fifo& operator=(Fifo&& rhs) = delete;

    static constexpr uint8_t OVERFLOW = 0xFFU;
    static constexpr uint8_t ERROR    = 0xFFU;

    //! @brief Fifoの状態を扱うときに使う器(うつわ)
    struct Status {
        uint32_t w;
        uint32_t r;
        uint32_t stock;
        uint32_t space;
        /**
         * @brief FIFOの現在の状態を返す
         * @details wとrとstock、といった3つ以上の状態が欲しいときに、
         * atomicな値を何度もリードするのは効率が悪いので、一気に得ら
         * れるようにこのコンストラクタを用意した。
         */
        Status(const Fifo* fifo) noexcept :
#if configNUMBER_OF_CORES >= 2
            w(fifo->m_wpos.load()), r(fifo->m_rpos.load()),
#else
            w(*(const_cast<volatile uint32_t*>(&fifo->m_wpos))),
            r(*(const_cast<volatile uint32_t*>(&fifo->m_rpos))),
#endif
            stock(), space()
        {
            stock = (w >= r) ? (w - r) : ((fifo->m_mask + 1U) + w - r);
            space = fifo->m_mask - stock;
        }
    };

    /**
     * @brief コンストラクタ
     * @param[out] ring リングバッファとして使う領域 !=nullptr
     * @param[in] bytes ringのバイト数 >=2 かつ 2の階乗
     * @details 溜め込める上限はbytes-1個である。
     * @note ringの寿命は、Fifoインスタンスと同じかより長い必要がある。
     * もしringのほうが寿命が短い場合、不正な読み書きを引き起こす可能性がある。 
     */
    Fifo(
        uint8_t* ring,
        size_t bytes) noexcept;

    /**
     * @brief コンストラクタ
     * @param[inout] ring リングバッファとして使う領域 !=nullptr
     * @details 溜め込める上限はringの中身の要素数-1個である。
     * @note ringの寿命は、Fifoインスタンスと同じかより長い必要がある。
     * もしringのほうが寿命が短い場合、不正な読み書きを引き起こす可能性がある。 
     */
    Fifo(
        Buffer* ring) noexcept;

    /**
     * @brief デストラクタ
     */
    virtual ~Fifo() noexcept {}

    /**
     * @brief データ列をFIFOに詰め込む
     * @param[in] buf データ列
     * @param[in] bytes bufのバイト数
     * @return 実際に詰め込むことのできたバイト数
     */
    size_t push(
        const uint8_t* buf,
        size_t bytes) noexcept;

    /**
     * @brief データ列をFIFOに詰め込む
     * @param[in] buf データ列
     * @param[in] bytes bufのバイト数
     * @return 実際に詰め込むことのできたバイト数
     */
    size_t push(
        const char* buf,
        size_t bytes) noexcept;

    /**
     * @brief 0終端文字列をFIFOに詰め込む
     * @param[in] buf 0終端文字列
     * @return 実際に詰め込むことのできたバイト数
     */
    size_t push(
        const char* buf) noexcept;

    /**
     * @brief 1文字をFIFOに詰め込む
     * @param[in] ch 文字
     * @return 実際に詰め込むことのできたバイト数
     * @details ISR上で呼ぶためのメソッドなので、コンパイラがインライン化
     * できるよう実装をヘッダで行っている。
     */
    size_t push(
        uint32_t ch) noexcept
    {
        const Status status(this);
        const uint32_t w = status.w;
        if (0U == status.space)
        {
            /* オーバーフローなので、前回の値をエラー値に書き換え、
             * m_wposは変化なしとする。 */
            m_ring[(w - 1) & m_mask] = OVERFLOW;
            return 0U;
        }

        m_ring[w] = static_cast<uint8_t>(ch);
#if configNUMBER_OF_CORES >= 2
        m_wpos.store((w + 1) & m_mask);
#else
        m_wpos = (w + 1) & m_mask;
#endif
        return 1U;
    }

    /**
     * @brief FIFOに十分な要素が溜まっているかを返す
     * @return FIFOの3/4以上溜まっていたらtrue
     */
    bool isEnough() const noexcept;

    /**
     * @brief 現在溜まっている数を返す
     * @return 溜まっている数 
     */
    size_t getStock() const noexcept;

    /**
     * @brief 現在の空きを返す
     * @return 空きの数 
     */
    size_t getSpace() const noexcept;

    /**
     * @brief 現在溜まっているぶんを回収する
     * @param[out] buf 情報の転送先 !=nullptr
     * @param[in] bytes bufの大きさ
     * @return 回収できた要素の数
     * @details bytesを超える書き込みは発生しない。もし、現在溜まっている
     * 要素数がbytesを上回る場合、FIFOの中には回収しきれなかった要素が残り、
     * 次回のpop()による回収の対象となる。
     * @note 結果がゼロ終端文字列になることは保証されない。ゼロ終端文字列
     * になるのは、push()する側がゼロを書いてきたときだけである。
     */
    size_t pop(
        uint8_t* buf,
        size_t bytes) noexcept;

    /**
     * @brief DMA転送に適した部分を得る
     * @param[out] addr DMA転送元アドレスを格納する変数のアドレス
     * @details DMACはリングバッファの構造を理解してくれないので、
     * リングバッファの最終アドレスを越えないよう手加減しつつ
     * DMA転送に適した部分を返す。
     * Fifoがカラの場合は戻り値として0を返す。
     */
    size_t getDmaTxCandidatePart(uint8_t **addr) noexcept;

    /**
     * @brief リードポインターを進める
     * @details Fifoへ、「直近のgetDmaTxCandidatePart()で対象となった
     * 領域はpop済みとみなしてよろしい」
     * と伝えるためのメソッド。DMA転送が完了したタイミングで呼び出さ
     * れることを想定している。
     */
    void markPoped() noexcept;
};

}
#endif
