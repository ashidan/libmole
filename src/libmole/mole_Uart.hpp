/**
 * @file
 * @brief UARTをラップするクラス
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_UART_HPP
#define MOLE_UART_HPP

#if defined(__RX__)
extern "C" {
#   include "r_sci_rx_if.h" // sci_ch_t
}
#else
#   include "hal_data.h" // uart headers
#endif
#include "libmole/mole_ByteIo.hpp" // ByteIo
#include "libmole/mole_Buffer.hpp" // Buffer

namespace mole {

namespace UartFactory {
#if defined(__RX__)
    /**
     * @brief Uartを作って返す
     * @param[in] ch チャネル番号(SCI_CHn)
     * @param[in] cfg R_SCI_Open()に与えるコンフィグレーション
     * @param[inout] txRing 送信リングバッファとして使う領域 !=nullptr
     * @param[inout] rxRing 受信リングバッファとして使う領域 !=nullptr
     * @param[in] indexToNotify Notifyに使うインデックス < configTASK_NOTIFICATION_ARRAY_ENTRIES
     * @details
     * Smart Configurator上のr_sci_rxにて、送信のDTC転送有効、受信は
     * デフォルト(DTC転送なし)にしたUARTを引数に与える必要がある。
     * @note txRingおよびrxRingの寿命は、Uartインスタンスと同じかより長い
     * 必要がある。もしringのほうが寿命が短い場合、不正な読み書きを引き
     * 起こす可能性がある。
     * この関数はヒープではなく特別な領域を消費する。
     * ユニットテストの場合を除き、deleteは考慮されていない。
     */
    ByteIo* create(
        sci_ch_t ch, const sci_cfg_t* cfg,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify = 0U) noexcept;
#else
    /**
     * @brief Uartを作って返す
     * @param[in] uart FSPのUARTインスタンス 送信DTC転送の有効化が必要
     * @param[inout] txRing 送信リングバッファとして使う領域 !=nullptr
     * @param[inout] rxRing 受信リングバッファとして使う領域 !=nullptr
     * @param[in] indexToNotify Notifyに使うインデックス < configTASK_NOTIFICATION_ARRAY_ENTRIES
     * @details
     * FSP Configuration上で送信のDTC転送有効、受信はデフォルト(DTC転送なし)
     * にしたUARTを引数に与える必要がある。
     * @note txRingおよびrxRingの寿命は、Uartインスタンスと同じかより長い
     * 必要がある。もしringのほうが寿命が短い場合、不正な読み書きを引き
     * 起こす可能性がある。
     * この関数はヒープではなく特別な領域を消費する。
     * ユニットテストの場合を除き、deleteは考慮されていない。
     */
    ByteIo* create(const uart_instance_t* uart,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify = 0U) noexcept;
#endif

#if defined(MOLE_USE_UTEST)
    /**
     * @brief 資源を開放する
     * @param[inout] Uartのインスタンス
     */
    void destroy(ByteIo* uart) noexcept;
#endif
} // end of UartFactory

} // end of mole
#endif
