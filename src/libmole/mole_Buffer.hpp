/**
 * @file
 * @brief バッファのアドレスとバイト数をまとめて扱う構造体を定義する
 * @details バッファのアドレスとバイト数を個別に扱っていると、メソッドの
 * 引数の数が増えてしまい扱いにくいので、この構造体を用意した。
 * この構造体を利用するには、例えば
 * static mole::BufferImpl<64> buf;
 * のように書いて、&bufを「Buffer*を引数にとる関数」へ渡す。
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_BUFFER_HPP
#define MOLE_BUFFER_HPP

#include <cstdint> // uint8_t 
#include <cstddef> // size_t

namespace mole {

/**
 * @brief バッファのアドレスとバイト数をまとめて扱う
 */
struct Buffer
{
    const size_t m_bytes; //!< バッファのバイト数
    uint8_t* const m_buf; //!< バッファのアドレス
    Buffer(size_t bytes, uint8_t* buf) noexcept :
        m_bytes(bytes), m_buf(buf)
    {}
};

/**
 * @brief Bufferの実体
 * @details
 * BYTES は >=2 かつ 2の階乗の必要がある。
 */
template<size_t BYTES>
struct BufferImpl final : public Buffer
{
    static_assert(BYTES >= 2, "");
    static_assert(0 == (BYTES & (BYTES>>1)), ""); // 2の階乗？

    uint8_t buf[BYTES];
    BufferImpl() noexcept : Buffer(BYTES, buf)
    {}
};

}
#endif
