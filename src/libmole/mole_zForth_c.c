/**
 * @file
 * @brief zForthのための、Cで実装したほうがよい部分の実装
 * @details
 * https://github.com/zevv/zForth/blob/master/src/atmega8/main.c
 * を参考にして作った、小規模マイコン用の実装。
 * C++ソースの中に配置すると zf_host_parse_num()
 * にnoexceptを付けることができないので、C言語で実装する。
 * zf_host_sys() に関しては、システム機能の実装をC++でしたい
 * ので、noexceptが付けられないことを許容しC++ソースに置く。
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "zforth/zforth.h" // zf_init()
#include <stdlib.h> // strtol()

zf_cell zf_host_parse_num(const char* buf)
{
    char* end = NULL;
    const zf_cell value = strtol(buf, &end, 0);
    if (*end != '\0')
    {
        zf_abort(ZF_ABORT_NOT_A_WORD);
    }
    return value;
}
