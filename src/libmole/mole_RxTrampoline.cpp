/**
 * @file
 * @brief コールバック関数から、ユーザー関数を引数付きで呼び出す
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#if defined(__RX__)

#include "libmole/mole_Trampoline.hpp"
#include "libmole/mole_trap.h" // MOLE_ASSERTNN()

namespace mole {

/* 動的に関数を作る。RXマイコンのABIは
 * https://www.renesas.com/jp/ja/document/mat/e-studio-rx-family-compiler-cc-rx-v20100-users-manual-rx-coding
 * 3.4.2 レジスタに関する規則 に載っているので、それに違反
 * しないように機械語を並べる必要がある。*/

#pragma GCC diagnostic ignored "-Wnarrowing"
Trampoline::Trampoline(vfunc_vp func, void* data) noexcept :
    m_code {
        0xFBU | (0x12U<<8U), // MOV.L #IMM:32 R1
        reinterpret_cast<uintptr_t>(data) >> 0U,
        reinterpret_cast<uintptr_t>(data) >> 16U,

        0xFBU | (0x52U<<8U), // MOV.L #IMM:32 R5
        reinterpret_cast<uintptr_t>(func) >>  0U,
        reinterpret_cast<uintptr_t>(func) >> 16U,

        0x7FU | (0x05U<<8U), // JMP R5
    }
{
    MOLE_ASSERTNN(func);
    MOLE_ASSERTNN(data);
}
#pragma GCC diagnostic warning "-Wnarrowing"

Trampoline::vfunc_v Trampoline::getCb(void) noexcept
{
    return reinterpret_cast<vfunc_v>(m_code);
}

}

#endif
