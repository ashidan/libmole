/**
 * @file
 * @brief CUI用のユーザー入力受付を提供する
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#include "libmole/mole_Shell.hpp"
#include "libmole/mole_trap.h" // MOLE_ASSERTNN()
#include "libmole/mole_Fifo.hpp" // Fifo
#include "libmole/mole_ByteIo.hpp" // ByteIo
#include "libmole/mole_misc.hpp" // flipArray()

#include <stdexcept> // runtime_error
#include <cstdio> // snprintf()
#include <cstring> // strlen()

#define ESC_GREEN   "\033[32m"
#define ESC_RED     "\033[31m"
#define ESC_DEFAULT "\033[39m"
#define PROMPT      ESC_GREEN "> " ESC_DEFAULT

namespace mole {

Shell::Shell(ByteIo* io, Interpreter* interp,
    uint8_t* buf, size_t bufBytes, uint32_t screenWidth,
    char promptReady, char promptScroll, char promptFull) noexcept :
    m_io(io), m_interp(interp), m_buf(buf), m_bufBytes(bufBytes),
    m_windowWidth(screenWidth - 1U - 1U), m_promptReady(promptReady),
    m_promptScroll(promptScroll), m_promptFull(promptFull)
{
    MOLE_ASSERTNN(io);
    MOLE_ASSERTNN(interp);
    MOLE_ASSERTNN(buf);
    MOLE_ASSERT(bufBytes >= 4, bufBytes);
    MOLE_ASSERT(screenWidth >= 4, screenWidth);
}

Shell::Shell(ByteIo* io, Interpreter* interp,
    uint8_t* buf, size_t bufBytes) noexcept :
    m_io(io), m_interp(interp), m_buf(buf), m_bufBytes(bufBytes),
    m_windowWidth(80U - 1U - 1U), m_promptReady('>'),
    m_promptScroll('<'), m_promptFull('!')
{
    MOLE_ASSERTNN(io);
    MOLE_ASSERTNN(interp);
    MOLE_ASSERTNN(buf);
    MOLE_ASSERT(bufBytes >= 4, bufBytes);

    /* 電源投入後すぐに入力履歴を参照した場合に、RAM上のゴミが表示
     * される問題があった。
     * その対策のため、バッファ末尾へナル文字を入れる。 */
    buf[bufBytes-1] = '\0';
}

[[noreturn]] void Shell::exec() noexcept
{
    // ゼロ終端するので、文字列の最大長は-1される
    const size_t commandLenMax = m_bufBytes - 1U;

    /* スクロール表示中は、左端の1文字がプロンプト'<'、
     * 右端の1文字がカーソル置き場になるので、先頭のほうは
     * 画面外に追い出されたように見える。 */

    m_io->write(PROMPT);

    uint32_t len    = 0U;
    bool     redraw = false;
    uint32_t escape = 0U;
    for (;;)
    {
        if (redraw)
        {
            redraw = false;
            if (len >= m_windowWidth)
            {
                if (len >= commandLenMax)
                {
                    m_io->write("\r" ESC_RED   "!" ESC_DEFAULT);
                }
                else
                {
                    m_io->write("\r" ESC_GREEN "<" ESC_DEFAULT);
                }
                m_io->write(&m_buf[len - m_windowWidth], m_windowWidth);
            }
            else
            {
                m_io->write("\r" PROMPT);
                m_io->write(m_buf, len);
            }
        }

        try
        {
            const uint8_t ch = m_io->read();
            if (1U == escape)
            {
                if ('[' == ch)
                {
                    escape++;
                }
                continue;
            }
            if (2U == escape)
            {
                escape = 0U;
                if ('A' == ch) // ↑キー
                {
                    m_buf[len] = '\0';
                    // 現時点の入力と入力履歴とを交換する
                    mole::flipArray(m_buf, m_bufBytes);
                    len = std::strlen(reinterpret_cast<char*>(m_buf));
                    m_io->write("\r\033[2K");
                    redraw = true;
                }
                continue;
            }

            if ('\r' == ch)
            {
                m_io->write("\r\n");

                m_buf[len] = '\0';
                m_interp->interpret(m_io, reinterpret_cast<char*>(m_buf));

                /* 組み込み環境ではRAMは貴重なので、入力バッファの後ろのほうを
                 * 1段階の入力履歴として転用する。 */
                mole::flipArray(m_buf, m_bufBytes);

                len = 0;
                redraw = true;
                continue;
            }

            if ('\b' == ch)
            {
                /* この行がないと、Backspaceを押してから↑を押したとき、
                 * 入力履歴に対してBackspace入力前の文字が付け足されて
                 * しまう可能性がある。この行があれば、入力履歴の後ろ
                 * のほうが削れるだけで済む。 */
                m_buf[len] = '\0';

                if (len > 0)
                {
                    if (len >= m_windowWidth)
                    {
                        redraw = true;
                    }
                    else
                    {
                        m_io->write("\b \b");
                    }
                    len--;
                }
                continue;
            }

            // 矢印キー ←↓→ は無視、↑は入力履歴の取り出し
            if ('\033' == ch)
            {
                escape++;
                continue;
            }

#if 0
            char str[3];
            std::snprintf(str, 3, "%02u", ch);
            m_io->write(str);
#endif
            if (len < commandLenMax)
            {
                m_buf[len] = ch;
                len++;
                if (len >= m_windowWidth)
                {
                    redraw = true;
                }
                else
                {
                    m_io->write(&ch, 1U);
                }
            }
        }
        catch (std::runtime_error& e)
        {
            // 通信の切断などのエラーがあった場合は、溜まっている文字を捨てる
            m_io->write("\r\n");
            len    = 0;
            redraw = true;
            escape = 0U;
        }
    }
}

}
