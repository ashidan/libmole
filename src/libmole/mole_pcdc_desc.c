/**
 * @file
 * @brief PCDC Descriptorを用意する
 * @details
 * RAの場合:
 * VENDORIDとPRODUCTIDを任意に書き換えたいが、Renesasに著作権のあるソースを
 * libmoleのリポジトリに含めたくはない。
 * なぜかr_usb_pcdc_descriptor.c.templateの変数群は定数ではなく変数であり、
 * 実行時に書き換えが可能である。
 * そこで、無理矢理includeして変数を用意したうえで、USBを使い始める前に
 * VENDORIDとPRODUCTIDの部分を書き換えることにした。
 * RXの場合:
 * Renesasに著作権のあるソースをlibmoleのリポジトリに含めたくなかったため、
 * RX用のdescriptorは
 * プロジェクトのsrc/renesas/r_usb_pcdc_descriptor.c.template
 * へ配置するというルールにした。
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 * @see https://www.renesas.com/jp/ja/products/microcontrollers-microprocessors/ra-cortex-m-mcus/ra-book
 */
#include "libmole/mole_config.h" // MOLE_CONFIG_PCDCUART
#if (MOLE_CONFIG_PCDCUART > 0U)

#if defined(__arm__)
#include "../ra/fsp/src/r_usb_pcdc/r_usb_pcdc_descriptor.c.template" // g_usb_descriptor
#endif // __arm__

#if defined(__RX__)
#include "r_usb_basic_if.h" // r_usb_pcdc_descriptor.c.templateに必要
#include "renesas/r_usb_pcdc_descriptor.c.template" // g_usb_descriptor
#endif // __RX__

#include "libmole/mole_pcdc_desc.h"

void mole_pcdc_setVidPid(uint16_t vendorId, uint16_t productId)
{
    g_apl_device[ 8] = vendorId & 0xFFU;
    g_apl_device[ 9] = (vendorId>>8U) & 0xFFU;
    g_apl_device[10] = productId & 0xFFU;
    g_apl_device[11] = (productId>>8U) & 0xFFU;
}

void mole_pcdc_setSerialNumber(uint32_t number)
{
    MOLE_ASSERT((1U<=number) && (number<=9U), number);
    g_cdc_string_descriptor6[26] = (uint8_t)number;
}

#endif // (MOLE_CONFIG_PCDCUART > 0U)
