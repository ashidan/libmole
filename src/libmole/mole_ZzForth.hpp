/**
 * @file
 * @brief ユーザー関数で拡張したzForthを作る
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_ZZFORTH_HPP
#define MOLE_ZZFORTH_HPP

#include "libmole/mole_zForth.hpp" // zForth

namespace mole {

/**
 * @brief zForthにいくつかのワードを追加し拡張したインタープリター
 */
class ZzForth : public mole::zForth {
public:
    /**
     * @brief コンストラクタ
     */
    ZzForth(mole::ByteIo* io) noexcept;

    ZzForth(const ZzForth&) = delete;
    ZzForth& operator=(const ZzForth&) = delete;
    ZzForth(ZzForth&& rhs) = delete;
    ZzForth& operator=(ZzForth&& rhs) = delete;

    virtual ~ZzForth() noexcept {}

    /**
     * @brief zForthのユーザー関数を実装する
     * @param io 入出力インスタンス
     * @param userFunc ユーザー関数番号 値域は128以上
     * @param input 不明
     */
    virtual void userFunc(mole::ByteIo* io, int32_t userFunc, const char* input) noexcept override;
};

}
#endif
