/**
 * @file
 * @brief PCDC UARTをラップするクラス
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 * @details プラットフォームのPCDC UART APIを包んで、容易に使えるようにする。
 */
#ifndef MOLE_PCDCUART_HPP
#define MOLE_PCDCUART_HPP

#if defined(__RX__)
extern "C" {
#   include "r_usb_basic_if.h" // usb_ctrl_t
}
#else
#   include "hal_data.h" // usb pcdc headers
#endif // __RX__

#include "libmole/mole_ByteIo.hpp" // ByteIo
#include "libmole/mole_Buffer.hpp" // Buffer

namespace mole {

namespace PcdcUartFactory {
#if defined(__RX__)
    /**
     * @brief PcdcUartを作って返す
     * @param[in] usb FITのUSBインスタンス !=nullptr
     * @param[in] cfg R_USB_Open()に与えるコンフィグレーション !=nullptr
     * @param[inout] txRing 送信リングバッファとして使う領域 !=nullptr
     * @param[inout] rxRing 受信リングバッファとして使う領域 !=nullptr
     * @param[in] priority UsbDaemonTskの優先度
     * @param[in] indexToNotify Notifyに使うインデックス < configTASK_NOTIFICATION_ARRAY_ENTRIES
     * @note txRingおよびrxRingの寿命は、PcdcUartインスタンスと同じかより長い
     * 必要がある。もしringのほうが寿命が短い場合、不正な読み書きを引き
     * 起こす可能性がある。
     * この関数はヒープではなく特別な領域を消費する。
     * ユニットテストの場合を除き、deleteは考慮されていない。
     * indexToNotifyの値は、いちタスクの中でユニークでなければならない。
     */
    ByteIo* create(
        const usb_ctrl_t* usb, const usb_cfg_t* cfg,
        Buffer* txRing, Buffer* rxRing,
        uint32_t priority,
        uint32_t indexToNotify = 0U) noexcept;
#else
    /**
     * @brief PcdcUartを作って返す
     * @param[in] usb FSPのUSBインスタンス !=nullptr
     * @param[inout] txRing 送信リングバッファとして使う領域 !=nullptr
     * @param[inout] rxRing 受信リングバッファとして使う領域 !=nullptr
     * @param[in] indexToNotify Notifyに使うインデックス < configTASK_NOTIFICATION_ARRAY_ENTRIES
     * @details
     * @note txRingおよびrxRingの寿命は、PcdcUartインスタンスと同じかより長い
     * 必要がある。もしringのほうが寿命が短い場合、不正な読み書きを引き
     * 起こす可能性がある。
     * この関数はヒープではなく特別な領域を消費する。
     * ユニットテストの場合を除き、deleteは考慮されていない。
     * indexToNotifyの値は、いちタスクの中でユニークでなければならない。
     */
    ByteIo* create(
        const usb_instance_t* usb,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify = 0U) noexcept;
#endif
} // end of PcdcUartfactory

} // end of mole
#endif
