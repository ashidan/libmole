/**
 * @file
 * @brief RXマイコンのGPIOピン設定をする
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#if defined(__RX__)

#include "libmole/mole_RxPin.hpp"
#include "libmole/mole_trap.h" // MOLE_ALWAYS()

namespace mole {

void RxPin::commit(void) const noexcept
{
    const gpio_port_pin_t pp = portAndPin();
    mpc_config_t config = {PSEL_POxx, irq(), ana()};
    const mpc_err_t err = R_MPC_Write(pp, &config);
    MOLE_ALWAYS(MPC_SUCCESS == err, err);
    if (out())
    {
        R_GPIO_PinWrite(pp, hl());
    }
    R_GPIO_PinDirectionSet(pp, out());
}

}

#endif
