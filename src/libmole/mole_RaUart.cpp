/**
 * @file
 * @brief UARTをラップするクラス
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#if defined(__arm__)
#include "libmole/mole_config.h" // MOLE_CONFIG_UART
#if (MOLE_CONFIG_UART > 0U)

#include "libmole/mole_Uart.hpp" // Uart
#include "libmole/mole_Fifo.hpp" // Fifo
#include "libmole/mole_trap.h" // MOLE_ALWAYS()

#include "FreeRTOS.h"
#include "semphr.h" // SemaphoreHandle_t
#include "timers.h" // xTimerPendFunctionCallFromISR()

#include <cstring> // strlen()
#include <atomic> // atomic_flag
#include <new> // new(std::nothrow)

namespace mole {

/**
 * @brief UARTをラップするクラス
 * @details プラットフォームのUART APIを包んで、容易に使えるようにする。
 * 複数のタスクから1つのUARTへ送信することも考慮して、排他には
 * task notificationでなくSemaphoreを使用している。
 * @note
 * 受信では、1文字受信するごとに割り込みが発生するため、例えば115.2kBPS
 * だと秒間11.52k回の割り込みになる。
 * 受信データがバースト的に入ってくる場合は、演算性能の悪化などの影響が
 * 発生しうる。
 */
class Uart : public ByteIo {
    static std::atomic_uint instances;

    uart_ctrl_t* const m_ctrl;
    const uart_api_t* const m_uart_api;
    uart_callback_args_t m_cb_body;
    mole::Fifo m_txFifo;
    mole::Fifo m_rxFifo;
    TaskHandle_t m_taskToNotify;
    TimerHandle_t m_rxTimer;
    std::atomic_flag m_rxTimerStarted; //!< Timerの2重起動防止
    SemaphoreHandle_t m_txDmaSem;
    StaticSemaphore_t m_txDmaSem_body;
    uint32_t m_indexToNotify;

public:
    /**
     * @brief コンストラクタ
     * @param[in] uart FSPのUARTインスタンス 送信DTC転送の有効化が必要
     * @param[inout] txRing 送信リングバッファとして使う領域 !=nullptr
     * @param[inout] rxRing 受信リングバッファとして使う領域 !=nullptr
     * @param[in] indexToNotify Notifyに使うインデックス < configTASK_NOTIFICATION_ARRAY_ENTRIES
     * @details
     * FSP Configuration上で送信のDTC転送有効、受信はデフォルト(DTC転送なし)
     * にしたUARTを引数に与える必要がある。
     * @note txRingおよびrxRingの寿命は、Uartインスタンスと同じかより長い
     * 必要がある。もしringのほうが寿命が短い場合、不正な読み書きを引き
     * 起こす可能性がある。
     */
    Uart(const uart_instance_t* uart,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify) noexcept;

    Uart(const Uart&) = delete;
    Uart& operator=(const Uart&) = delete;
    Uart(Uart&& rhs) = delete;
    Uart& operator=(Uart&& rhs) = delete;

    virtual ~Uart() noexcept {}

    virtual void write(
        const uint8_t* buf,
        size_t bytes) noexcept override;

    virtual void write(
        const char* buf,
        size_t bytes) noexcept override;

    virtual void write(
        const char* buf) noexcept override;

    virtual size_t read(uint8_t* buf, size_t bytes) noexcept override;

    virtual size_t read(char* buf, size_t bytes) noexcept override;

    virtual uint8_t read() override;

    virtual int8_t readc() override;

    virtual void setReaderTask(TaskHandle_t task) noexcept override;

private:
    static void *operator new(size_t bytes, const std::nothrow_t&) noexcept
    {
        (void)bytes;
        static uintptr_t ram[MOLE_CONFIG_UART * sizeof(Uart)/sizeof(uintptr_t)];

        const uint32_t myIndex = instances.fetch_add(1U);
        MOLE_ALWAYS(myIndex < MOLE_CONFIG_UART, myIndex);
        return &ram[myIndex * sizeof(Uart)/sizeof(uintptr_t)];
    }

    /**
     * @brief newと対になる、nothrow用のdelete
     * @param ptr
     * @details new と対になる delete。デストラクタは暗黙では呼び出されない。
     */
    static void operator delete(void* ptr, const std::nothrow_t&) noexcept
    {
#if defined(MOLE_USE_UTEST)
        /* 必要であれば、ここでデストラクタに相当する処理を行う。
         * Uart* const p = static_cast<Uart*>(ptr);
         */

        (void)ptr;
        const uint32_t count = instances.fetch_sub(1U);
        MOLE_ALWAYS(count >= 1, count);
#else
        (void)ptr;
#endif
    }

    /**
     * @brief nothrow用deleteを用意した場合、デストラクタから呼ばれるふつうのdeleteも用意する必要がある
     * @param ptr 無視される
     */
    static void operator delete(void* ptr) noexcept
    {
        (void)ptr;
    }

    static void kickTxDma(void* inst, uint32_t ignore) noexcept;
    static void rxNotifyTimerCb(TimerHandle_t handle) noexcept;
    static void callback(uart_callback_args_t* p_args) noexcept;

public:
    static Uart* create(const uart_instance_t* uart,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify) noexcept
    {
        return new(std::nothrow) Uart(uart,
            txRing, rxRing,
            indexToNotify);
    }
#if defined(MOLE_USE_UTEST)
    static void destroy(ByteIo* uart) noexcept
    {
        Uart::operator delete(uart, std::nothrow);
    }
#endif
};

std::atomic_uint Uart::instances;

void Uart::kickTxDma(void* inst, uint32_t ignore) noexcept
{
    (void)ignore;
    Uart* const uart = static_cast<Uart*>(inst);
    uint8_t* addr = nullptr;
    const size_t bytes = uart->m_txFifo.getDmaTxCandidatePart(&addr);
    uart->m_uart_api->write(uart->m_ctrl, addr, bytes);
}

void Uart::rxNotifyTimerCb(TimerHandle_t handle) noexcept
{
    Uart* const uart = static_cast<Uart*>(pvTimerGetTimerID(handle));

    /* 受けとる側のタスクは、通知があったことのみに興味があり、
     * 通知の値には興味がない。 */
    const auto rc = xTaskNotifyIndexed(
        uart->m_taskToNotify, uart->m_indexToNotify, 0U, eNoAction);
    MOLE_ALWAYS(pdTRUE == rc, rc);
    uart->m_rxTimerStarted.clear();
}

// 静的メンバーメソッド
void Uart::callback(uart_callback_args_t* p_args) noexcept
{
    BaseType_t woken = pdFALSE;

    // p_args->p_contextはconst void*型なので、面倒なキャストが必要になる。
    Uart* const uart = static_cast<Uart*>(
        const_cast<void*>(p_args->p_context));

    switch (p_args->event)
    {
        case UART_EVENT_TX_DATA_EMPTY:
            {
                uart->m_txFifo.markPoped();
            }
            break;
        case UART_EVENT_TX_COMPLETE:
            /* TX_COMPLETEでuart_api->write()してしまうと、このコールバック関数が
             * リターンしたあとの処理で割り込みイネーブルなどの操作があり、
             * 予測不可能な振舞いをする。
             * そこで、xTimerPendFunctionCall()を利用して「ISRが終了したあと」
             * にuart_api->write()をする。 */
            {
                const size_t bytes = uart->m_txFifo.getStock();
                if (0U == bytes)
                {
                    xSemaphoreGiveFromISR(uart->m_txDmaSem, &woken);
                }
                else
                {
                    const auto rc = xTimerPendFunctionCallFromISR(
                        kickTxDma, uart, 0U, &woken);
                    MOLE_ALWAYS(pdPASS == rc, rc);
                }
            }
            break;
        case UART_EVENT_RX_CHAR:
            {
                // uart_api->read()を呼んでいなくても、このイベントは発生する
                uart->m_rxFifo.push(p_args->data);

                /* 1文字受信するたびに「受信データを欲しがっているタスク」へ
                 * コンテキストスイッチするのは無駄なので行わない。
                 * その代わりに、Timerを開始し1ms後にnotifyする */
                if (false == uart->m_rxTimerStarted.test_and_set())
                {
                    /* test_and_set()する前がfalseであったとき、
                     * m_rxTimerStartedはtrueに変わり、そしてここへ到達する */
                    const auto rc = xTimerStartFromISR(uart->m_rxTimer,
                       &woken);
                    MOLE_ALWAYS(pdPASS == rc, rc);
                }
            }
            break;
        case UART_EVENT_ERR_PARITY: // ↓
        case UART_EVENT_ERR_FRAMING: // ↓
        case UART_EVENT_BREAK_DETECT:
            {
                /* 「何らかのエラーが発生したこと」だけは、受信データを欲しがって
                 * いるタスクから検知できるようにする */
                uart->m_rxFifo.push(Fifo::ERROR);
            }
            break;
        default:
            break;
    }
    portYIELD_FROM_ISR(woken);
}

Uart::Uart(const uart_instance_t* uart,
    Buffer* txRing, Buffer* rxRing,
    uint32_t indexToNotify) noexcept :
    m_ctrl(uart->p_ctrl), m_uart_api(uart->p_api),
    m_cb_body(),
    m_txFifo(txRing), m_rxFifo(rxRing),
    m_taskToNotify(xTaskGetCurrentTaskHandle()),
    m_rxTimer(nullptr), m_rxTimerStarted(ATOMIC_FLAG_INIT),
    m_txDmaSem(), m_txDmaSem_body(),
    m_indexToNotify(indexToNotify)
{
    MOLE_ASSERTNN(uart);
    MOLE_ASSERT(indexToNotify < configTASK_NOTIFICATION_ARRAY_ENTRIES, indexToNotify);
    // ringとbytesの検査はFifoコンストラクタで行うので、ここでは行わない

    m_txDmaSem = xSemaphoreCreateBinaryStatic(
        &m_txDmaSem_body);
    MOLE_ALWAYS(nullptr != m_txDmaSem, m_txDmaSem);
    const auto rc = xSemaphoreGive(m_txDmaSem); // 1回Takeできる状態にしておく
    MOLE_ALWAYS(pdTRUE == rc, rc);

    // 受信用Timerを作っておく
    {
        static StaticTimer_t timerBody;
        m_rxTimer = xTimerCreateStatic(
            "rxNotify",
            pdMS_TO_TICKS(1),
            pdFALSE,             // uxAutoReload
            static_cast<void*>(this), // pvTimerID
            rxNotifyTimerCb,
            &timerBody
            );
        MOLE_ALWAYS(nullptr != m_rxTimer, m_rxTimer);
        // 起動はISRから行うので、ここでは作っておくだけ。
    }

    /* uart->p_cfgのほうにはコールバック関数を書いていないので、
     * open()したあとにcallbackSet()の順で呼び出さないと
     * open()のせいでそれ以前のcallbackSet()が無効化されてしまう。 */
    fsp_err_t err = m_uart_api->open(uart->p_ctrl, uart->p_cfg);
    MOLE_ALWAYS(FSP_SUCCESS == err, err);

    err = m_uart_api->callbackSet(
        m_ctrl,
        callback,
        this,
        &m_cb_body);
    MOLE_ALWAYS(FSP_SUCCESS == err, err);
}

void Uart::write(
    const uint8_t* buf,
    size_t bytes) noexcept
{
    MOLE_ASSERTNN(buf);
    if (0U == bytes) return;

    for (;;)
    {
        const size_t actual = m_txFifo.push(buf, bytes);

        if (pdTRUE == xSemaphoreTake(m_txDmaSem, 0U))
        {
            /* セマフォを獲得できたということはDMA転送は未起動なので、
             * このタイミングで起動する。 */
            uint8_t *addr = nullptr;
            const size_t writable = m_txFifo.getDmaTxCandidatePart(&addr);
            /* xSemaphoreTake()がリターンする前に、callback()のなかで
             * データ送出を終えている可能性があり、writable==0がありうる */
            if (writable > 0U)
            {
                m_uart_api->write(this->m_ctrl, addr, writable);
            }
            else
            {
                const auto rc = xSemaphoreGive(m_txDmaSem);
                MOLE_ALWAYS(pdTRUE == rc, rc);
            }
        }
        else
        {
            /* セマフォを獲得できなかったということは今はDMA転送中であり、
             * 放っておけばcallbackがDMA転送を再起動してくれるので何もしない。*/
        }

        if (actual >= bytes)
        {
            return;
        }

        // fifoの空きが十分でなく、押し込みきれなかったとき
        buf   += actual;
        bytes -= actual;

        /* 少し待って、push()に再挑戦する。
         * 本当は時間でなくイベントを待ちたいのであるが、適当な
         * ものがないので時間にしている。 */
        vTaskDelay(pdMS_TO_TICKS(1));
    }
}

void Uart::write(
    const char* buf,
    size_t bytes) noexcept
{
    write(reinterpret_cast<const uint8_t*>(buf), bytes);
}

void Uart::write(
    const char* buf) noexcept
{
    write(reinterpret_cast<const uint8_t*>(buf), std::strlen(buf));
}

size_t Uart::read(uint8_t* buf, size_t bytes) noexcept
{
    size_t actual = 0U;
    for (;;)
    {
        actual = m_rxFifo.pop(buf, bytes);
        if (actual > 0)
        {
            break;
        }

        // 通知が来るまで寝て待つ
        ulTaskNotifyTakeIndexed(m_indexToNotify, pdTRUE, portMAX_DELAY);
    }
    return actual;
}

size_t Uart::read(char* buf, size_t bytes) noexcept
{
    return read(reinterpret_cast<uint8_t*>(buf), bytes);
}

uint8_t Uart::read()
{
    uint8_t data;
    read(&data, 1U);
    return data;
}

int8_t Uart::readc()
{
    uint8_t data;
    read(&data, 1U);
    return static_cast<int8_t>(data);
}

void Uart::setReaderTask(TaskHandle_t task) noexcept
{
    MOLE_ASSERTNN(task);
    m_taskToNotify = task;
}

namespace UartFactory {
    ByteIo* create(const uart_instance_t* uart,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify) noexcept
    {
        return Uart::create(uart,
            txRing, rxRing,
            indexToNotify);
    }

#if defined(MOLE_USE_UTEST)
    void destroy(ByteIo *uart) noexcept
    {
        Uart::destroy(uart);
    }
#endif
} // end of UartFactory

} // end of mole

#endif // (MOLE_CONFIG_UART > 0U)
#endif // __arm__
