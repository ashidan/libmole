/**
 * @file
 * @brief UARTをラップするクラス
 * @details
 * このソースの利用のためには、リンカーオプションへ以下を加える必要がある。
 * -Wl,--wrap=rxi_handler
 * -Wl,--wrap=eri_handler
 * -Wl,--wrap=txi_handler
 * -Wl,--wrap=tei_handler
 * その理由は、コールバックではなく乗っ取った???_handlerを使うことによって
 * 処理の高速化を図っているから。
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#if defined(__RX__)
#include "libmole/mole_config.h" // MOLE_CONFIG_UART
#if (MOLE_CONFIG_UART > 0U)

#include "libmole/mole_Uart.hpp" // Uart
#include "libmole/mole_Fifo.hpp" // Fifo
#include "libmole/mole_trap.h" // MOLE_ALWAYS()

#include "FreeRTOS.h"
#include "semphr.h" // SemaphoreHandle_t
#include "timers.h" // TimerHandle_t

#include <cstring> // strlen()
#include <atomic> // atomic_flag
#include <new> // new(std::nothrow)

extern "C" {
#include "r_sci_rx_platform.h" // st_sci_ch_ctrl
}

namespace mole {


/**
 * @brief UARTをラップするクラス
 * @details プラットフォームのUART APIを包んで、容易に使えるようにする。
 * 複数のタスクから1つのUARTへ送信することも考慮して、排他には
 * task notificationでなくSemaphoreを使用している。
 * @note
 * 受信では、1文字受信するごとに割り込みが発生するため、例えば115.2kBPS
 * だと秒間11.52k回の割り込みになる。
 * 受信データがバースト的に入ってくる場合は、演算性能の悪化などの影響が
 * 発生しうる。
 */
class Uart : public ByteIo {
    static std::atomic_uint instances;

    sci_hdl_t m_handle;
    TaskHandle_t m_taskToNotify;
    StaticSemaphore_t m_txDmaSem_body;
    uint32_t m_indexToNotify;

public:
    /** __wrap_???_handler()から参照したいが、friendは効かないのでpublicにする
     * https://stackoverflow.com/questions/7809468/friendship-with-extern-c-function-seems-to-require-to-qualify-name */
    mole::Fifo m_txFifo;
    mole::Fifo m_rxFifo;
    TimerHandle_t m_rxTimer;
    std::atomic_flag m_rxTimerStarted; //!< Timerの2重起動防止
    SemaphoreHandle_t m_txDmaSem;

public:
    /**
     * @brief コンストラクタ
     * @param[in] ch チャネル番号(SCI_CHn)
     * @param[in] cfg R_SCI_Open()に与えるコンフィグレーション
     * @param[inout] txRing 送信リングバッファとして使う領域 !=nullptr
     * @param[inout] rxRing 受信リングバッファとして使う領域 !=nullptr
     * @param[in] indexToNotify Notifyに使うインデックス < configTASK_NOTIFICATION_ARRAY_ENTRIES
     * @details
     * Smart Configurator上のr_sci_rxにて、送信のDTC転送有効、受信は
     * デフォルト(DTC転送なし)にしたUARTを引数に与える必要がある。
     * @note txRingおよびrxRingの寿命は、Uartインスタンスと同じかより長い
     * 必要がある。もしringのほうが寿命が短い場合、不正な読み書きを引き
     * 起こす可能性がある。
     */
    Uart(
        sci_ch_t ch, const sci_cfg_t* cfg,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify) noexcept;

    Uart(const Uart&) = delete;
    Uart& operator=(const Uart&) = delete;
    Uart(Uart&& rhs) = delete;
    Uart& operator=(Uart&& rhs) = delete;

    virtual ~Uart() noexcept {}

    virtual void write(
        const uint8_t* buf,
        size_t bytes) noexcept override;

    virtual void write(
        const char* buf,
        size_t bytes) noexcept override;

    virtual void write(
        const char* buf) noexcept override;

    virtual size_t read(uint8_t* buf, size_t bytes) noexcept override;

    virtual size_t read(char* buf, size_t bytes) noexcept override;

    virtual uint8_t read() override;

    virtual int8_t readc() override;

    virtual void setReaderTask(TaskHandle_t task) noexcept override;

private:
    static void *operator new(size_t bytes, const std::nothrow_t&) noexcept
    {
        (void)bytes;
        static uintptr_t ram[MOLE_CONFIG_UART * sizeof(Uart)/sizeof(uintptr_t)];

        const uint32_t myIndex = instances.fetch_add(1U);
        MOLE_ALWAYS(myIndex < MOLE_CONFIG_UART, myIndex);
        return &ram[myIndex * sizeof(Uart)/sizeof(uintptr_t)];
    }

    /**
     * @brief newと対になる、nothrow用のdelete
     * @param ptr
     * @details new と対になる delete。デストラクタは暗黙では呼び出されない。
     */
    static void operator delete(void* ptr, const std::nothrow_t&) noexcept
    {
#if defined(MOLE_USE_UTEST)
        /* 必要であれば、ここでデストラクタに相当する処理を行う。
         * Uart* const p = static_cast<Uart*>(ptr);
         */

        (void)ptr;
        const uint32_t count = instances.fetch_sub(1U);
        MOLE_ALWAYS(count >= 1, count);
#else
        (void)ptr;
#endif
    }

    /**
     * @brief nothrow用deleteを用意した場合、デストラクタから呼ばれるふつうのdeleteも用意する必要がある
     * @param ptr 無視される
     */
    static void operator delete(void* ptr) noexcept
    {
        (void)ptr;
    }

    static void rxNotifyTimerCb(TimerHandle_t handle) noexcept;
public:
    static Uart* create(sci_ch_t ch, const sci_cfg_t* cfg,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify) noexcept
    {
        return new(std::nothrow) Uart(ch, cfg,
            txRing, rxRing,
            indexToNotify);
    }
#if defined(MOLE_USE_UTEST)
    static void destroy(ByteIo* uart) noexcept
    {
        Uart::operator delete(uart, std::nothrow);
    }
#endif
};

std::atomic_uint Uart::instances;

void Uart::rxNotifyTimerCb(TimerHandle_t handle) noexcept
{
    Uart* const uart = reinterpret_cast<Uart*>(pvTimerGetTimerID(handle));

    /* 受けとる側のタスクは、通知があったことのみに興味があり、
     * 通知の値には興味がない。 */
    const auto rc = xTaskNotifyIndexed(
        uart->m_taskToNotify, uart->m_indexToNotify, 0U, eNoAction);
    MOLE_ALWAYS(pdTRUE == rc, rc);
    uart->m_rxTimerStarted.clear();
}

Uart::Uart(
    sci_ch_t ch, const sci_cfg_t* cfg,
    Buffer* txRing, Buffer* rxRing,
    uint32_t indexToNotify) noexcept :
    m_handle(nullptr),
    m_taskToNotify(xTaskGetCurrentTaskHandle()),
    m_txDmaSem_body(),
    m_indexToNotify(indexToNotify),
    m_txFifo(txRing), m_rxFifo(rxRing),
    m_rxTimer(nullptr), m_rxTimerStarted(ATOMIC_FLAG_INIT),
    m_txDmaSem()
{
    MOLE_ASSERT(indexToNotify < configTASK_NOTIFICATION_ARRAY_ENTRIES, indexToNotify);
    // ringとbytesの検査はFifoコンストラクタで行うので、ここでは行わない
    m_txDmaSem = xSemaphoreCreateBinaryStatic(
        &m_txDmaSem_body);
    MOLE_ALWAYS(nullptr != m_txDmaSem, m_txDmaSem);
    const auto rc = xSemaphoreGive(m_txDmaSem); // 1回Takeできる状態にしておく
    MOLE_ALWAYS(pdTRUE == rc, rc);

    // 受信用Timerを作っておく
    {
        static StaticTimer_t timerBody;
        m_rxTimer = xTimerCreateStatic(
            "rxNotify",
            pdMS_TO_TICKS(1),
            pdFALSE,             // uxAutoReload
            static_cast<void*>(this), // pvTimerID
            rxNotifyTimerCb,
            &timerBody
            );
        MOLE_ALWAYS(nullptr != m_rxTimer, m_rxTimer);
        // 起動はISRから行うので、ここでは作っておくだけ。
    }

    sci_cfg_t variable_cfg = *cfg; // R_SCI_Open()は定数を受け取れない

    /* R_SCI_Open()からu_rx_data.queの書き換えまでの間にUART割り込みが
     * あったとしても、保留にする。 */
    taskDISABLE_INTERRUPTS();
    {
        const sci_err_t err = R_SCI_Open(ch, SCI_MODE_ASYNC, &variable_cfg, nullptr, &m_handle);
        MOLE_ALWAYS(SCI_SUCCESS == err, err);

        //! 未使用のメンバーへ、無理矢理Uartへのポインタを覚えこませる
        m_handle->u_rx_data.que = reinterpret_cast<byteq_hdl_t>(this);
    }
    taskENABLE_INTERRUPTS();
}

void Uart::write(
    const uint8_t* buf,
    size_t bytes) noexcept
{
    MOLE_ASSERTNN(buf);
    if (0U == bytes) return;

    for (;;)
    {
        const size_t actual = m_txFifo.push(buf, bytes);

        if (pdTRUE == xSemaphoreTake(m_txDmaSem, 0U))
        {
            /* セマフォを獲得できたということはDMA転送は未起動なので、
             * このタイミングで起動する。 */
            uint8_t *addr = nullptr;
            const size_t writable = m_txFifo.getDmaTxCandidatePart(&addr);
            /* xSemaphoreTake()がリターンする前に、callback()のなかで
             * データ送出を終えている可能性があり、writable==0がありうる */
            if (writable > 0U)
            {
                R_SCI_Send(m_handle, addr, writable);
            }
            else
            {
                const auto rc = xSemaphoreGive(m_txDmaSem);
                MOLE_ALWAYS(pdTRUE == rc, rc);
            }
        }
        else
        {
            /* セマフォを獲得できなかったということは今はDMA転送中であり、
             * 放っておけば__wrap_tei_handler()がDMA転送を再起動してくれる
             * ので何もしない。*/
        }

        if (actual >= bytes)
        {
            return;
        }

        // fifoの空きが十分でなく、押し込みきれなかったとき
        buf   += actual;
        bytes -= actual;

        /* 少し待って、push()に再挑戦する。
         * 本当は時間でなくイベントを待ちたいのであるが、適当な
         * ものがないので時間にしている。 */
        vTaskDelay(pdMS_TO_TICKS(1));
    }
}

void Uart::write(
    const char* buf,
    size_t bytes) noexcept
{
    write(reinterpret_cast<const uint8_t*>(buf), bytes);
}

void Uart::write(
    const char* buf) noexcept
{
    write(reinterpret_cast<const uint8_t*>(buf), std::strlen(buf));
}

size_t Uart::read(uint8_t* buf, size_t bytes) noexcept
{
    size_t actual = 0U;
    for (;;)
    {
        actual = m_rxFifo.pop(buf, bytes);
        if (actual > 0)
        {
            break;
        }

        // 通知が来るまで寝て待つ
        ulTaskNotifyTakeIndexed(m_indexToNotify, pdTRUE, portMAX_DELAY);
    }
    return actual;
}

size_t Uart::read(char* buf, size_t bytes) noexcept
{
    return read(reinterpret_cast<uint8_t*>(buf), bytes);
}

uint8_t Uart::read()
{
    uint8_t data;
    read(&data, 1U);
    return data;
}

int8_t Uart::readc()
{
    uint8_t data;
    read(&data, 1U);
    return static_cast<int8_t>(data);
}

void Uart::setReaderTask(TaskHandle_t task) noexcept
{
    MOLE_ASSERTNN(task);
    m_taskToNotify = task;
}

namespace UartFactory {
    ByteIo* create(sci_ch_t ch, const sci_cfg_t* cfg,
        Buffer* txRing, Buffer* rxRing,
        uint32_t indexToNotify) noexcept
    {
        return Uart::create(ch, cfg,
            txRing, rxRing,
            indexToNotify);
    }

#if defined(MOLE_USE_UTEST)
    void destroy(ByteIo *uart) noexcept
    {
        Uart::destroy(uart);
    }
#endif
} // end of UartFactory

} // end of mole

/**
 * @brief rxi_handler乗っ取り関数
 * @details 本来rxi_handlerが呼ばれるタイミングでこの関数が呼ばれる。
 * r_sci_rx標準の受信割り込み処理ではbyteqに書き込む処理があり、
 * 115.2kbpsのとき1msに11.52回呼ばれる割り込みとしては無駄なので、
 * 乗っ取って必要最小限の処理に置き換える。
 */
extern "C" void __wrap_rxi_handler(sci_hdl_t hdl) noexcept
{
    mole::Uart* uart = reinterpret_cast<mole::Uart*>(hdl->u_rx_data.que);

    const uint8_t byte = hdl->rom->regs->RDR;
    uart->m_rxFifo.push(byte);

    /* 1文字受信するたびに「受信データを欲しがっているタスク」へ
     * コンテキストスイッチするのは無駄なので行わない。
     * その代わりに、Timerを開始し1ms後にnotifyする */
    if (false == uart->m_rxTimerStarted.test_and_set())
    {
        BaseType_t woken = pdFALSE;
        /* test_and_set()する前がfalseであったとき、
         * m_rxTimerStartedはtrueに変わり、そしてここへ到達する */
        const auto rc = xTimerStartFromISR(uart->m_rxTimer,
           &woken);
        MOLE_ALWAYS(pdPASS == rc, rc);
        portYIELD_FROM_ISR(woken);
    }
}

/**
 * @brief eri_handler乗っ取り関数
 * @details 本来eri_handlerが呼ばれるタイミングでこの関数が呼ばれる。
 */
extern "C" void __wrap_eri_handler(sci_hdl_t hdl) noexcept
{
    // エラーフラグのクリア
    {
        static const uint8_t PER  = 1U<<3U; // パリティ
        static const uint8_t FER  = 1U<<4U; // フレーミング
        static const uint8_t ORER = 1U<<5U; // オーバーラン
        uint8_t ssr = hdl->rom->regs->SSR.BYTE;
        if (ssr & PER)  { ssr &= ~PER; }
        if (ssr & FER)  { ssr &= ~FER; }
        if (ssr & ORER) { ssr &= ~ORER; }
        hdl->rom->regs->SSR.BYTE = ssr;
    }

    mole::Uart* uart = reinterpret_cast<mole::Uart*>(hdl->u_rx_data.que);
    /* 「何らかのエラーが発生したこと」だけは、受信データを欲しがって
     * いるタスクから検知できるようにする */
    uart->m_rxFifo.push(mole::Fifo::ERROR);
}

extern "C" void __real_txi_handler(sci_hdl_t hdl) noexcept;
extern "C" void __real_tei_handler(sci_hdl_t hdl) noexcept;

/**
 * @brief txi_handler乗っ取り関数
 * @details 本来txi_handlerが呼ばれるタイミングでこの関数が呼ばれる。
 */
extern "C" void __wrap_txi_handler(sci_hdl_t hdl) noexcept
{
    mole::Uart* uart = reinterpret_cast<mole::Uart*>(hdl->u_rx_data.que);
    // txi_handlerが呼ばれた時点で、txFifoからTDRへの転送は済んでいる
    uart->m_txFifo.markPoped();

    __real_txi_handler(hdl);
}

#if !defined(SCI_CFG_TEI_INCLUDED)
#   error r_sci_rx - Transmit end interrupt must be enabled.
#endif
/**
 * @brief tei_handler乗っ取り関数
 * @details 本来tii_handlerが呼ばれるタイミングでこの関数が呼ばれる。
 */
extern "C" void __wrap_tei_handler(sci_hdl_t hdl) noexcept
{
    __real_tei_handler(hdl);

    BaseType_t woken = pdFALSE;
    mole::Uart* uart = reinterpret_cast<mole::Uart*>(hdl->u_rx_data.que);
    const size_t bytes = uart->m_txFifo.getStock();
    if (0U == bytes)
    {
        xSemaphoreGiveFromISR(uart->m_txDmaSem, &woken);
    }
    else
    {
        /* r_sci_rxドライバでの処理は__real_tei_handler()にてすべて終わっている
         * ので、xTimerPendFunctionCall()を利用せずに、ここでR_SCI_Send()を
         * 呼んでも問題ない */
        uint8_t* addr = nullptr;
        const size_t bytes = uart->m_txFifo.getDmaTxCandidatePart(&addr);
        R_SCI_Send(hdl, addr, bytes);
    }
    portYIELD_FROM_ISR(woken);
}

#endif // (MOLE_CONFIG_UART > 0U)
#endif // __RX__
