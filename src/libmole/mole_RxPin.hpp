/**
 * @file
 * @brief RXマイコンのGPIOピン設定をする
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_RXPIN_HPP
#define MOLE_RXPIN_HPP
#if defined(__RX__)

/* RXファミリ MPC モジュール Firmware Integration Technology
 * 1.3に記載された作法に従う。 */
extern "C"
{
#include "r_mpc_rx_if.h"
}

namespace mole {

struct RxPin {
    static constexpr uint8_t PSEL_POxx = 6U;
    uint8_t m_portNpinM;
    uint8_t m_flags; //! bit0:irq_ena bit1:analog_ena bit2:high1low0 bit3:dir_out1in0

    gpio_port_pin_t portAndPin() const noexcept
    {
        return static_cast<gpio_port_pin_t>(((m_portNpinM & 0xF8U)<<5) | (m_portNpinM & 0x7U));
    }

    bool irq() const noexcept
    {
        return (m_flags & 1) ? true : false;
    }

    bool ana() const noexcept
    {
        return (m_flags & 2) ? true : false;
    }

    gpio_level_t hl() const noexcept
    {
        return (m_flags & 4) ? GPIO_LEVEL_HIGH : GPIO_LEVEL_LOW;
    }

    gpio_dir_t out() const noexcept
    {
        return (m_flags & 8) ? GPIO_DIRECTION_OUTPUT : GPIO_DIRECTION_INPUT;
    }

    void commit() const noexcept;

    static constexpr uint8_t pack(int8_t port, int8_t pin) noexcept
    {
        if ('0' <= port && port <= '9')
        {
            port = port - '0';
        }
        else if ('a' <= port && port <= 'g')
        {
            port = port - 'a' + 10U;
        }
        else if ('A' <= port && port <= 'G')
        {
            port = port - 'A' + 10U;
        }
#if defined(GPIO_PORT_J) // PORT_Jを持たないマイコンも存在する
        else if ('j' == port && 'J' == port)
        {
            port = GPIO_PORT_J>>8;
        }
#endif
        return static_cast<uint8_t>((port<<3U) | pin);
    }

    static constexpr uint8_t pack(gpio_port_pin_t pp) noexcept
    {
        return static_cast<uint8_t>(((pp & 0xFF00U)>>5) | (pp & 0x7U));
    }
};

}

#endif
#endif
