/**
 * @file
 * @brief スレッドセーフ化のため、malloc系関数を自分で用意する
 * @details
 * ライブラリで提供され自分でソースを書き換えることができない箇所で
 * malloc()やfree()が呼ばれていてもスレッドセーフになるように、
 * malloc()系の関数を自分で用意したもので置き換える。
 *
 * 置き換えてしまえば、BSPのHeap Sizeは0にできる。
 *
 * gnu ldの--wrapオプションを使わずとも、libg_nano.aの作りとして
 * シンボルがweakになっているので、weakでないシンボルを自分で用意
 * すればそちらが使用されるようだ。
 * 参考
 * https://www.gnu.org/software/libc/manual/html_node/Replacing-malloc.html
 * https://www.iar.com/ja/knowledge/support/technical-notes/general/using-rtos-heap-memory
 *
 * realloc()では、元の領域のサイズがどれくらいかを知るにはheap_4.cの
 * 実装を知る必要があるので、コピーを行うバイト数として新しい領域の
 * サイズを使う。
 * ただし、それではrealloc()がコピー動作中にヒープ領域の末端を越えて
 * リードアクセスすることがありうる。
 * そこで、configAPPLICATION_ALLOCATED_HEAPを1にし、ヒープが配置された
 * アドレスをこの mole_wrap.c 自身で参照可能にする。
 *
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "FreeRTOS.h"
#include <stdlib.h> // size_t, malloc()
#include <stdalign.h> // _Alignas()
#include <string.h> // memcpy()
#include "libmole/mole_trap.h" // MOLE_ASSERT()

#if configAPPLICATION_ALLOCATED_HEAP == 0
#   error configAPPLICATION_ALLOCATED_HEAP must set 1
#endif

_Alignas(portBYTE_ALIGNMENT) uint8_t ucHeap[configTOTAL_HEAP_SIZE];

void* malloc(size_t xSize)
{
    /* heap_4.cで実装されたpvPortMalloc()は、事前の初期化無しで
     * いきなり呼び出すことができる。heap_5.cだとそうはいかない。
     * FreeRTOSスケジューラーの起動前でもpvPortMalloc()は使用可能。 */
    return pvPortMalloc(xSize);
}

void free(void* pv)
{
    vPortFree(pv);
}

// weakなので、RAだとheap_4.cにある実装が、RXだとこの実装が使用される。
__attribute__((weak)) void* pvPortCalloc(size_t xNum, size_t xSize)
{
	void* const p = pvPortMalloc(xNum * xSize);
	if (p)
	{
		memset(p, 0, xNum * xSize);
	}
	return p;
}

void* calloc(size_t xNum, size_t xSize)
{
    return pvPortCalloc(xNum, xSize);
}

void* realloc(void* oldptr, size_t xSize)
{
    void* const newptr = pvPortMalloc(xSize);
    if (newptr != NULL && oldptr != NULL)
    {
        // ヒープ領域の末端を越えるアクセスを防止
        static const uint8_t* const end = &ucHeap[configTOTAL_HEAP_SIZE];
        MOLE_ASSERT(&ucHeap[0] <= (uint8_t*)oldptr && (uint8_t*)oldptr <= end, oldptr);
        const size_t tailBytes = (size_t)(end - (uint8_t*)oldptr);
        const size_t copyBytes = (tailBytes < xSize) ? tailBytes : xSize;
        memcpy(newptr, oldptr, copyBytes);
        vPortFree(oldptr);
    }
    return newptr;
}
