/**
 * @file
 * @brief zForthとの橋渡しをする
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_ZFORTH_HPP
#define MOLE_ZFORTH_HPP

#include "libmole/mole_ByteIo.hpp" // ByteIo
#include "libmole/mole_Interpreter.hpp" // Interpreter
#include "FreeRTOS.h"
#include "semphr.h" // SemaphoreHandle_t

namespace mole {

/**
 * @brief zForthインタプリタとの橋渡しをする
 */
class zForth : public Interpreter {
private:
    SemaphoreHandle_t m_mutex;
    StaticSemaphore_t m_mutexEntity;
    int32_t           m_lastUserFunc;

public:
    /**
     * @brief コンストラクタ
     * @param[inout] io 入出力インスタンス
     * @details ioは、zForthインスタンスに記憶させるためではなく、
     * zForthインスタンスの生成仮定において文字出力を可能にするために
     * 与える。
     */
    zForth(ByteIo* io) noexcept;

    /**
     * @brief コンストラクタ
     * @param[inout] io 入出力インスタンス
     * @param[in] words 「\0」で区切られたワードの羅列
     * @details zForthの派生クラスから使われることを意図した
     * コンストラクタであり、コンストラクタの中でwordsを解釈し登録する。
     * @note 個々のワードは16文字以内でなければならない。
     */
    zForth(ByteIo* io, const char* words) noexcept;

    zForth(const zForth&) = delete;
    zForth& operator=(const zForth&) = delete;
    zForth(zForth&& rhs) = delete;
    zForth& operator=(zForth&& rhs) = delete;

    virtual ~zForth() noexcept {}

    virtual void interpret(ByteIo* io, const char* buf) noexcept override;

    /**
     * @brief このメソッドをoverrideすることで、zForthのユーザー関数を実装する
     * @param[inout] io 入出力インスタンス
     * @param[in] userFunc ユーザー関数番号 値域は0以上
     * @param[in] input 用途不明 (zForthにおける未知の部分)
     */
    virtual void userFunc(ByteIo* io, int32_t userFunc, const char* input) noexcept;
};

}
#endif
