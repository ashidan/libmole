/**
 * @file
 * @brief インタープリターのインタフェースクラス
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef MOLE_INTERPRETER_HPP
#define MOLE_INTERPRETER_HPP

#include "libmole/mole_ByteIo.hpp" // ByteIo

namespace mole {

/**
 * @brief インタフェース
 */
class Interpreter
{
public:
    /**
     * @brief コンストラクタ
     */
    Interpreter() noexcept {}

    /**
     * @brief デストラクタ
     */
    virtual ~Interpreter() noexcept {}

    /**
     * @brief 文字列を解釈する
     * @param[inout] io 入出力に使うインスタンス !=nullptr
     * @param[in] buf 文字列 !=nullptr
     * @details
     * ゼロ終端文字列を解釈する。
     */
    virtual void interpret(
        ByteIo* io, const char* buf) noexcept = 0;
};

}
#endif
