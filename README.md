libmole
===============

Copyright 2024 molelord

RenesasのRなんとか系マイコン用のライブラリです。git submoduleで参照されることを想定して開発しています。

この libmole をsubmoduleとして参照するプロジェクトが、 https://gitlab.com/ashidan/uc-r にあります。

改行および文字コードは、自分で作ったファイルはLFかつUTF-8BOM無し、自動生成ツールなどが生成したファイルは作られたときのまま、にしています。

ソース群を Doxygen に食わせて作ったドキュメントが https://ashidan.daemon.asia/html/ にあります。

## コンセプト

自分でソフトウェアを全部作るのは面倒すぎるので、Renesas提供の自動生成ツールやライブラリを利用する。
ただし、納得いかない部分はリンカーオプションによる関数乗っ取りやe2studioのbuild steps機能を使ったスクリプト実行などで自動的に何とかして、自動生成ソースやライブラリソースを手作業で直した結果それが勝手に元通りにされてブチ切れそうになるのを回避する。
です。

## 命名規則

| 対象 | 規則 | 備考 |
| ---- | ---- | ---- |
| 構造体 | `Foo` | |
| typedef名 | `〜_td` | `_t` は処理系で予約されているため |
| グローバル変数 | `g_foo_bar` | |
| ファイルスコープの変数 | `f_foo` | |
| クラスのメンバー変数 | `m_foo` | |
| クラスの静的メンバー変数 | `s_foo` | |
| マクロ | `MOLE_FOO_BAR` | 大文字をアンダーバーで連結 |
| インクルードガード | `FILENAME_HPP` | |
| StaticAPI用の実体 | `〜Entity` | xTaskCreateStatic()などで使う |
| キューの記憶域 | `〜QItems[n]` | |

ほか、名前空間を汚さないために、クラスや関数は極力 namespace mole 以下に配置しています。

## Doxygenコメントにおけるポインター渡しのin/out/inoutのルール

### in

仮引数へ与えたポインターの指す先に関して、ReadはしますがWriteはしません。

### out

仮引数へ与えたポインターの指す先に関して、Readはせず、Writeをします。
エラーなどの発生時は、Writeしないでリターンすることもありえます。
なお、指す先でなくポインター自体は(アドレス自体は)使用します。

### inout

仮引数へ与えたポインターの指す先に関して、ReadとWriteの両方を行います。
例えば、カウンタを進めたり、文字列中の小文字を探して大文字に変えたりといった
Read-Modify-Writeをします。

## 引数や戻り値のチェック

関数入口での引数のチェックは MOLE_ASSERT() および MOLE_ASSERTNN()
で行っています。NN はNot Nullの意味です。

関数の戻り値のチェックは MOLE_ALWAYS() および MOLE_ALWAYSNN() で行っています。

MOLE_ASSERT()系 はデバッグビルドでのみ有効でリリースビルドでは無効化されますが、 MOLE_ALWAYS()系 はリリースビルドでも有効です。

## ペリフェラルを隠蔽するクラス

ペリフェラルを扱うクラス群に関しては、コンストラクタを間接的に呼び出す設計になっています。

例えばUARTを使いたい場合、 `プロジェクト/src/app` に app_mole_config.h を配置して MOLE_CONFIG1のいちばん下の桁に「UARTをインスタンシエートしたい数」を書き、
`#include "libmole/mole_Uart.hpp` したうえで以下のようにして `ByteIo` インタフェースを介して操作してください。

```
mole::ByteIo* io = mole::UartFactory::create(〜);
```


実例は uc-Rプロジェクトの各サンプルプログラムの app/app_launcher_tsk.cpp にあります。
