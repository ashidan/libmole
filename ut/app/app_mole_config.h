/**
 * @file
 * @brief libmoleの持つ機能の取捨選択をする
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 * @see https://gitlab.com/ashidan/libmole/-/blob/main/src/libmole/mole_config.h
 * @note
 * これはDoxygenでのドキュメント自動生成用に用意したファイルである。
 * 本来は、libmoleを利用する側のプロジェクトにおいて、任意の設定を書いたものを
 * src/app/ へ配置する。
 */
#ifndef APP_MOLE_CONFIG_H
#define APP_MOLE_CONFIG_H

#ifndef MOLE_CONFIG1
//                      ? -+ +- ?
//                    ? -+ | | +- ?
//                  ? -+ | | | | +- UART
//                     | | | | | |
#define MOLE_CONFIG1  000000000011U
//                      | | | | |
//                   ? -+ | | | +- USB PCDC UART
//                     ? -+ | +- ?
//                          +- ?
#endif

#endif
