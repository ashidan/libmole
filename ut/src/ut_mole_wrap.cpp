/**
 * @file
 * @brief realloc独自実装のテスト
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "CppUTest/TestHarness.h"
#include <cstdlib> // malloc()
#include <cstring> // memcmp()

TEST_GROUP(realloc)
{
    uint32_t* oldptr;
    uint32_t* expected;
    uint32_t* newptr;
    static constexpr size_t oldLen = 10;
    void setup()
    {
        static constexpr size_t bytes = oldLen * sizeof(*oldptr);
        oldptr = static_cast<uint32_t*>(std::malloc(bytes));
        for (uint32_t i = 0; i < oldLen; i++)
        {
            oldptr[i] = 0x123456789U*i;
        }
        expected = static_cast<uint32_t*>(std::malloc(bytes));
        memcpy(expected, oldptr, bytes);

        newptr = NULL;
    }

    void teardown()
    {
        free(newptr);
        free(expected);
        // oldptrの指す領域は、realloc()によって開放済み
    }

    void* realloc(void* pv, size_t xSize)
    {
        void* newptr = malloc(xSize);
        if (newptr != NULL && pv != NULL)
        {
            std::memcpy(newptr, pv, xSize);
            free(pv);
        }
        return newptr;
    }
};

TEST(realloc, reduce)
{
    newptr = static_cast<uint32_t*>(realloc(oldptr, 5 * sizeof(*oldptr)));
    CHECK_TRUE(newptr != NULL);
    if (newptr != NULL)
    {
        MEMCMP_EQUAL(expected, newptr, 5 * sizeof(*oldptr));
    }
}

TEST(realloc, inflate)
{
    newptr = static_cast<uint32_t*>(realloc(oldptr, 17 * sizeof(*oldptr)));
    CHECK_TRUE(newptr != NULL);
    if (newptr != NULL)
    {
        MEMCMP_EQUAL(expected, newptr, oldLen * sizeof(*oldptr));
    }
}

TEST(realloc, same)
{
    newptr = static_cast<uint32_t*>(realloc(oldptr, oldLen * sizeof(*oldptr)));
    CHECK_TRUE(newptr != NULL);
    if (newptr != NULL)
    {
        MEMCMP_EQUAL(expected, newptr, oldLen * sizeof(*oldptr));
    }
}

#if 0 // サイズが0でも、戻り値はNULLとは限らない
TEST(realloc, to_zero)
{
    newptr = static_cast<uint32_t*>(realloc(oldptr, 0));
    CHECK_TRUE(newptr == NULL);
}
#endif

TEST(realloc, from_zero)
{
    free(oldptr);
    newptr = static_cast<uint32_t*>(realloc(NULL, 13));
    CHECK_TRUE(newptr != NULL);
}
