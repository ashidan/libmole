/**
 * @file
 * @brief mole::Fifoのユニットテスト
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#include "libmole/mole_Fifo.hpp"
#include "libmole/mole_misc.hpp" // elementsOf()
#include "CppUTest/TestHarness.h"

TEST_GROUP(Fifo)
{
    void setup()
    {
    }

    void teardown()
    {
    }
};

// pushしたぶん、getStockに作用するか？
TEST(Fifo, push)
{
    uint8_t ring[8];
    mole::Fifo fifo = mole::Fifo(ring, sizeof(ring));

    static const uint8_t src[] = {'A'};
    fifo.push(&src[0], 1);
    const size_t stock = fifo.getStock();
    UNSIGNED_LONGS_EQUAL(1, stock);
    const size_t space = fifo.getSpace();
    UNSIGNED_LONGS_EQUAL(sizeof(ring)-1-stock, space);

    const mole::Fifo::Status status = mole::Fifo::Status(&fifo);
    UNSIGNED_LONGS_EQUAL(stock, status.stock);
    UNSIGNED_LONGS_EQUAL(space, status.space);
}

// いちどでpushしきれない場合の挙動は正しいか？
TEST(Fifo, push2)
{
    uint8_t ring[8];
    mole::Fifo fifo = mole::Fifo(ring, sizeof(ring));

    static const uint8_t src[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'};
    const size_t take1 = fifo.push(src, sizeof(src));
    UNSIGNED_LONGS_EQUAL(7, take1); // ringの大きさ-1までしか入らない

    {
        uint8_t buf[4];
        const size_t elems = fifo.pop(buf, sizeof(buf));
        UNSIGNED_LONGS_EQUAL(4, elems);
    }

    const size_t stock = fifo.getStock();
    UNSIGNED_LONGS_EQUAL(7 - 4, stock);

    const size_t take2 = fifo.push(src + take1, sizeof(src) - take1);
    UNSIGNED_LONGS_EQUAL(2, take2);

    {
        uint8_t buf[5];
        const size_t elems = fifo.pop(buf, sizeof(buf));
        static const uint8_t expected[] = {'E', 'F', 'G', 'H', 'I'};
        MEMCMP_EQUAL(expected, buf, elems);
    }
}

// pushしたものをpopできるか
TEST(Fifo, pop)
{
    uint8_t ring[8];
    mole::Fifo fifo = mole::Fifo(ring, sizeof(ring));

    static const uint8_t src[] = {'A', 'B', 'C'};
    fifo.push(src, 3);

    uint8_t buf[8];
    {
        const size_t elems = fifo.pop(buf, sizeof(buf));
        UNSIGNED_LONGS_EQUAL(3, elems);
        static const uint8_t expected[] = {'A', 'B', 'C'};
        MEMCMP_EQUAL(expected, buf, 3);
    }

    {
        const size_t elems = fifo.pop(buf, sizeof(buf));
        UNSIGNED_LONGS_EQUAL(0, elems); // 回収済みなので0になるはず
    }
}

// 溜まっている数の計算は正しいか
TEST(Fifo, getStock)
{
    uint8_t ring[4];
    mole::Fifo fifo = mole::Fifo(ring, sizeof(ring));

    static const uint8_t src[] = {'A', 'B', 'C', 'D', 'E'};
    fifo.push(&src[0], 1);
    const bool enough1 = fifo.isEnough();
    CHECK(enough1 == false);

    fifo.push(&src[1], 1);
    const bool enough2 = fifo.isEnough();
    CHECK(enough2 == false);

    fifo.push(&src[2], 1);
    const bool enough3 = fifo.isEnough();
    CHECK(enough3 == true);

    {
        const size_t stock = fifo.getStock();
        UNSIGNED_LONGS_EQUAL(3, stock);
    }

    {
        uint8_t buf[8];
        const size_t elems = fifo.pop(buf, sizeof(buf));
        UNSIGNED_LONGS_EQUAL(3, elems);
    }

    // この2バイトのpushで、writeに関して一回転する
    fifo.push(&src[3], 2);
    const bool enough4 = fifo.isEnough();
    CHECK(enough4 == false);

    // 一回転したあとでも、溜まっている数の計算は正しいか？
    {
        const size_t stock = fifo.getStock();
        UNSIGNED_LONGS_EQUAL(2, stock);

        uint8_t buf[4];
        const size_t elems = fifo.pop(buf, sizeof(buf));
        UNSIGNED_LONGS_EQUAL(2, elems);
        static const uint8_t expected[] = {'D', 'E'};
        MEMCMP_EQUAL(expected, buf, 2);
    }
}

TEST(Fifo, txCandidate)
{
    uint8_t ring[8];
    mole::Fifo fifo = mole::Fifo(ring, sizeof(ring));
    static const uint8_t src[] = {'A', 'B', 'C', 'D', 'E'};
    fifo.push(src, 5);

    size_t bytes = 0;
    uint8_t* addr = nullptr;
    bytes = fifo.getDmaTxCandidatePart(&addr);
    UNSIGNED_LONGS_EQUAL(5, bytes);
    UNSIGNED_LONGS_EQUAL(5, fifo.getStock());
    CHECK(addr != nullptr);

    fifo.markPoped();
    UNSIGNED_LONGS_EQUAL(0, fifo.getStock());

    // リングが一回転しているので、リングの終わりまでしか取り出せないはず
    fifo.push(src, 5);
    addr = nullptr;
    bytes = fifo.getDmaTxCandidatePart(&addr);
    UNSIGNED_LONGS_EQUAL(3, bytes);
    CHECK(addr != nullptr);

    fifo.push(src, 1);
    fifo.markPoped();
    UNSIGNED_LONGS_EQUAL(5 - 3 + 1, fifo.getStock());

    fifo.push(src, 4);
    addr = nullptr;
    bytes = fifo.getDmaTxCandidatePart(&addr);
    UNSIGNED_LONGS_EQUAL(5 - 3 + 1 + 4, bytes);
    UNSIGNED_LONGS_EQUAL(5 - 3 + 1 + 4, fifo.getStock());
    CHECK(addr != nullptr);
}

// リードポインターがリングの末尾にあるとき
TEST(Fifo, txCandidate2)
{
    uint8_t ring[8];
    mole::Fifo fifo = mole::Fifo(ring, sizeof(ring));
    static const uint8_t src[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
    fifo.push(src, 7);

    size_t bytes = 0;
    uint8_t* addr = nullptr;
    bytes = fifo.getDmaTxCandidatePart(&addr);
    UNSIGNED_LONGS_EQUAL(7, bytes);
    UNSIGNED_LONGS_EQUAL(7, fifo.getStock());
    CHECK(addr != nullptr);

    fifo.markPoped();
    UNSIGNED_LONGS_EQUAL(0, fifo.getStock());

    // リングの末尾に1要素入れる
    fifo.push(src, 1);
    addr = nullptr;
    bytes = fifo.getDmaTxCandidatePart(&addr);
    UNSIGNED_LONGS_EQUAL(1, bytes);
    CHECK(addr != nullptr);

    fifo.markPoped();
    UNSIGNED_LONGS_EQUAL(0, fifo.getStock());
}

// オーバーフロー時の挙動は意図どおりか
TEST(Fifo, overflow)
{
    uint8_t ring[8];
    mole::Fifo fifo = mole::Fifo(ring, sizeof(ring));
    static const uint8_t src[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
    for (uint32_t i = 0; i < mole::elementsOf(src); i++)
    {
        fifo.push(src[i]);
    }

    uint8_t buf[8];
    size_t bytes = 0;
    bytes = fifo.pop(buf, 7);
    UNSIGNED_LONGS_EQUAL(7, bytes);
    static const uint8_t expected[] =
        {'A', 'B', 'C', 'D', 'E', 'F', mole::Fifo::OVERFLOW};
    MEMCMP_EQUAL(expected, buf, 7);
}
