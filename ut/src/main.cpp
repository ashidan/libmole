/**
 * @file
 *
 * https://cpputest.github.io/manual.html
 * を引用して作成した。
 */

#include "CppUTest/CommandLineTestRunner.h"

int main(int ac, char** av)
{
   return CommandLineTestRunner::RunAllTests(ac, av);
}
