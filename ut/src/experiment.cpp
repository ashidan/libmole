/**
 * @file
 * @brief C++の挙動を確かめるための実験場
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "CppUTest/TestHarness.h"
#include "libmole/mole_misc.hpp" // elementsOf()
#include <cstring> // memcpy()
#include <cstdio> // snprintf()

// 配列のとある要素から末尾要素までのバイト数は？
TEST_GROUP(area)
{
    void setup()
    {
    }

    void teardown()
    {
    }
};

TEST(area, uint8array)
{
    uint8_t buf[32];
    uint8_t* end = &buf[32];

    void* oldptr = &buf[29];
    CHECK_TRUE(end - (uint8_t*)oldptr >= 0);
    const size_t tailBytes = (size_t)(end - (uint8_t*)oldptr);
    UNSIGNED_LONGS_EQUAL(3 * sizeof(uint8_t), tailBytes);
}

TEST(area, int32array)
{
    int32_t buf[20];
    int32_t* end = &buf[20];

    void* oldptr = &buf[1];
    CHECK_TRUE((uint8_t*)end - (uint8_t*)oldptr >= 0);
    const size_t tailBytes = (size_t)((uint8_t*)end - (uint8_t*)oldptr);
    UNSIGNED_LONGS_EQUAL(19 * sizeof(int32_t), tailBytes);
}

static void copy(uint8_t* __restrict__ dest, const uint8_t* __restrict__ src,
    uint32_t w, uint32_t r, size_t ringBytes)
{
    if (r == w)
    {
        ; // 何もしない
    }
    else if (r < w)
    {
        std::memcpy(dest, &src[r], w - r);
    }
    else // r > w
    {
        const size_t firstHalf  = ringBytes - r;
        const size_t secondHalf = w - 0U;
        std::memcpy(dest, &src[r], firstHalf);
        if (secondHalf > 0U)
        {
            std::memcpy(&dest[firstHalf], &src[0], secondHalf);
        }
    }
}

// リングバッファをコピー r < w のとき
TEST(area, copy_0_r_w_end)
{
    uint8_t src[32];
    uint8_t dest[8] = {0};
    for (uint32_t i = 0; i < mole::elementsOf(src); i++)
    {
        src[i] = i;
    }

    const uint32_t len = mole::elementsOf(dest);
    uint32_t w = 5+len;
    uint32_t r = 5;
    copy(dest, src, w, r, sizeof(src));
    uint8_t expected[len];
    for (uint32_t i = 0; i < len; i++)
    {
        expected[i] = r + i;
    }
    MEMCMP_EQUAL(expected, dest, len);
}

// リングバッファをコピー r == w のとき
TEST(area, copy_0_rw_end)
{
    uint8_t src[32];
    uint8_t dest[8] = {0};
    for (uint32_t i = 0; i < mole::elementsOf(src); i++)
    {
        src[i] = i;
    }

    const uint32_t len = mole::elementsOf(dest);
    uint32_t w = 7;
    uint32_t r = 7;
    copy(dest, src, w, r, sizeof(src));
    static constexpr uint8_t expected[len] = {0};
    MEMCMP_EQUAL(expected, dest, len);
}

// リングバッファをコピー w < r のとき
TEST(area, copy_0_w_r_end)
{
    uint8_t src[32];
    uint8_t dest[8] = {0};
    for (uint32_t i = 0; i < mole::elementsOf(src); i++)
    {
        src[i] = i;
    }

    const uint32_t len = mole::elementsOf(dest);
    uint32_t w =  5;
    uint32_t r = 29;
    copy(dest, src, w, r, sizeof(src));
    static constexpr uint8_t expected[len] = {
        29, 30, 31, 0, 1, 2, 3, 4,
    };
    MEMCMP_EQUAL(expected, dest, len);
}

// リングバッファをコピー w==0 < r のとき
TEST(area, copy_0w_r_end)
{
    uint8_t src[32];
    uint8_t dest[8] = {0};
    for (uint32_t i = 0; i < mole::elementsOf(src); i++)
    {
        src[i] = i;
    }

    const uint32_t len = mole::elementsOf(dest);
    uint32_t w =  0;
    uint32_t r = 24;
    copy(dest, src, w, r, sizeof(src));
    static constexpr uint8_t expected[len] = {
        24, 25, 26, 27, 28, 29, 30, 31,
    };
    MEMCMP_EQUAL(expected, dest, len);
}

// バッファの長さが足りないときのsnprintfの挙動を調べる
TEST_GROUP(snprintf)
{
    void setup()
    {
    }

    void teardown()
    {
    }
};

/*
 * nは、snprintfに対して「書き込んでも構わない最大のバイト数」
 * を伝える。バッファの大きさを与えればオーバーフローは発生し
 * えない。また、必ず0終端の文字列がバッファへ書き込まれるので
 * バッファの末尾を突き抜けた文字列ができてしまうこともない。
 */
// ゼロなら\0すら書かれないはず
TEST(snprintf, zero)
{
    char buf[32];
    for (size_t i = 0; i < mole::elementsOf(buf); i++)
    {
        buf[i] = -1;
    }
    const int len = std::snprintf(buf, 0, "%d", 123); 
    LONGS_EQUAL(3, len); // nが十分大きければ書けたであろう文字数
    CHECK_EQUAL(-1, buf[0]);
}

// 1なら\0だけ書かれるはず
TEST(snprintf, one)
{
    char buf[32];
    for (size_t i = 0; i < mole::elementsOf(buf); i++)
    {
        buf[i] = -1;
    }
    const int len = std::snprintf(buf, 1, "%d", 123); 
    LONGS_EQUAL(3, len); // nが十分大きければ書けたであろう文字数
    CHECK_EQUAL('\0', buf[0]);
    CHECK_EQUAL(-1, buf[1]);
}

// 3なら2文字と\0が書かれるはず
TEST(snprintf, three)
{
    char buf[32];
    for (size_t i = 0; i < mole::elementsOf(buf); i++)
    {
        buf[i] = -1;
    }
    const int len = std::snprintf(buf, 3, "%d", 123); 
    LONGS_EQUAL(3, len); // nが十分大きければ書けたであろう文字数
    CHECK_EQUAL('1', buf[0]);
    CHECK_EQUAL('2', buf[1]);
    CHECK_EQUAL('\0', buf[2]);
    CHECK_EQUAL(-1, buf[3]);
}

