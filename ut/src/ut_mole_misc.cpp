/**
 * @file
 * @brief mole_miscのテスト
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "CppUTest/TestHarness.h"
#include <cstdlib> // malloc()
#include <cstring> // memcmp()
#include "libmole/mole_misc.hpp" // flipArray()

TEST_GROUP(misc)
{
    void setup()
    {
    }

    void teardown()
    {
    }
};

TEST(misc, flip_odd)
{
    uint8_t buf[] = {
        1, 2, 3, 4, 5
    };
    static const uint8_t expected[] = {
        5, 4, 3, 2, 1
    };

    mole::flipArray(buf, mole::elementsOf(buf));
    MEMCMP_EQUAL(expected, buf, mole::elementsOf(buf));
}

TEST(misc, flip_even)
{
    uint8_t buf[] = {
        11, 22, 33, 44
    };
    static const uint8_t expected[] = {
        44, 33, 22, 11
    };

    mole::flipArray(buf, mole::elementsOf(buf));
    MEMCMP_EQUAL(expected, buf, mole::elementsOf(buf));
}
